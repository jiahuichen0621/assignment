﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC_Simulator
{
    class Lookback : ParentOption, ISimulator
    {
        public Lookback()
        {

        }
        public Lookback(Underlying under) : base(under)
        {
        }



        // Methods that will return the Option price and Standard Error based on a certain payoff matrix and the information given by the users.
        public double[] getprice_sd(double S0, double SK, double r, double vol, double T, int steps, int trials)
        {
            Simulator simu = new Simulator(S0, SK, r, vol, T, steps, trials);
            simu.call = call;
            simu.control = control;
            simu.anti = anti;
            simu.multithread = multithread;
            if (anti)
            {
                simu.allsims = stockmat(S0, SK, r, vol, T, steps, trials);
                simu.allsims_anti = stockmat_anti(S0, SK, r, vol, T, steps, trials);
            }
            else
            {
                simu.allsims = stockmat(S0, SK, r, vol, T, steps, trials);
            }

            double[,] payoff = simu.getPayoff_lookback();
            double sum1 = 0;
            double sum2 = 0;
            for (int i = 0; i < trials; i++)
            {
                sum1 = sum1 + payoff[i, 0];
                sum2 = sum2 + Math.Pow(payoff[i, 0], 2);
            }
            double optionpx = sum1 / trials * Math.Exp(-r * T);
            double sd = Math.Sqrt((sum2 - sum1 * sum1 / trials) * Math.Exp(-2 * r * T) / (trials - 1));
            double se = sd / Math.Sqrt(trials);
            double[] result = new double[2] { optionpx, se };

            return result;
        }
    }
}

