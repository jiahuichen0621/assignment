﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MC_Simulator
{
    class Underlying
    {
        public double S0 { get; set; }
        public double SK { get; set; }
        public double r { get; set; }
        public double vol { get; set; }
        public double T { get; set; }
        public int steps { get; set; }
        public int trials { get; set; }
        public Underlying()
        {
            S0 = 0;
            SK = 0;
            r = 0;
            vol = 0;
            T = 0;
            steps = 0;
            trials = 0;
        }
        public Underlying(double[] x)
        {
            S0 = x[0];
            SK = x[1];
            r = x[2];
            vol = x[3];
            T = x[4];
            steps = int.Parse(Convert.ToString(x[5]));
            trials = int.Parse(Convert.ToString(x[6]));

        }


    }
}
