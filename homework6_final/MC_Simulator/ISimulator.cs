﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC_Simulator
{
    // An interface which force those classes who implement it must implement getprice_sd() method.
    interface ISimulator
    {


        double[] getprice_sd(double S0, double SK, double r, double vol, double T, int steps, int trials);



    }
}

