﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MC_Simulator
{
    class Simulator
    {
        public double S0 { get; set; }
        public double SK { get; set; }
        public double r { get; set; }
        public double vol { get; set; }
        public double T { get; set; }
        public static int steps { get; set; }
        public static int trials { get; set; }
        public int cores = Environment.ProcessorCount;
        public static double beta1 = -1;
        public bool control;
        public bool call;
        public bool multithread;
        public bool anti;
        public int barrier_type;
        public double barrier_price;
        public double digital_rebate;
        public double[,] allsims = new double[trials, steps];
        public double[,] allsims_anti = new double[trials, steps];
        public Simulator(double _S0, double _SK, double _r, double _vol, double _T, int _steps, int _trials)
        {
            S0 = _S0;
            SK = _SK;
            r = _r;
            vol = _vol;
            T = _T;
            steps = _steps;
            trials = _trials;

        }
        public Simulator(EU euro)
        {
            S0 = euro.S0;
            SK = euro.SK;
            r = euro.r;
            vol = euro.vol;
            T = euro.T;
            steps = euro.steps;
            trials = euro.trials;
        }
        public Simulator(Asian asia)
        {
            S0 = asia.S0;
            SK = asia.SK;
            r = asia.r;
            vol = asia.vol;
            T = asia.T;
            steps = asia.steps;
            trials = asia.trials;
        }
        public double[,] getPayoff_euro()
        {

            double[,] CT = new double[trials, 1];
            double[,] CT_1 = new double[trials, 1];
            double[,] CT_2 = new double[trials, 1];
            double dt = 1.0 * T / (steps - 1);
            double nudt = (r - 0.5 * vol * vol) * dt;
            double sigsdt = vol * Math.Sqrt(dt);
            double erddt = Math.Exp(r * dt);

            // Calculate how many rows need to be handled by each processor (Multi threading)
            int percore;
            if (trials % cores != 0)
            {
                percore = (trials + (cores - trials % cores)) / cores;
            }
            else { percore = trials / cores; }


            Action<object> Payoff = x =>
            {
                long para = Convert.ToInt32(x);
                long start = para;
                long ending = para + percore;
                if (ending > trials) ending = trials;

                // Get the first payoff matrix CT_1 using random_mat_1 random matrix.
                for (long j = start; j < ending; j++)
                {
                    double cv = 0;
                    for (int i = 1; i < steps; i++)
                    {
                        double t = (i - 1) * dt;
                        double d1 = (Math.Log(allsims[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                        double delta = 0;
                        if (call)
                        {
                            delta = CumDensity(d1);
                        }
                        else { delta = CumDensity(d1) - 1; }
                        cv = cv + delta * (allsims[j, i] - allsims[j, i - 1] * erddt);
                    }
                    if (control)
                    {
                        if (call)
                        {
                            CT_1[j, 0] = Math.Max(0, allsims[j, steps - 1] - SK) + beta1 * cv;
                        }
                        else
                        {
                            CT_1[j, 0] = Math.Max(0, SK - allsims[j, steps - 1]) + beta1 * cv;
                        }
                    }
                    else
                    {
                        if (call)
                        {
                            CT_1[j, 0] = Math.Max(0, allsims[j, steps - 1] - SK);
                        }
                        else
                        {
                            CT_1[j, 0] = Math.Max(0, SK - allsims[j, steps - 1]);
                        }
                    }

                }


                // Get the second payoff matrix CT_2 using random_mat_2 random matrix. Random_mat_2 has negative elements compared to random_mat_1.
                if (anti)
                {
                    for (long j = start; j < ending; j++)
                    {
                        double cv = 0;
                        for (int i = 1; i < steps; i++)
                        {
                            double t = (i - 1) * dt;
                            double d1 = (Math.Log(allsims_anti[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                            double delta = 0;
                            if (call)
                            {
                                delta = CumDensity(d1);
                            }
                            else { delta = CumDensity(d1) - 1; }
                            cv = cv + delta * (allsims_anti[j, i] - allsims_anti[j, i - 1] * erddt);
                        }
                        if (control)
                        {
                            if (call)
                            {
                                CT_2[j, 0] = Math.Max(0, allsims_anti[j, steps - 1] - SK) + beta1 * cv;
                            }
                            else
                            {
                                CT_2[j, 0] = Math.Max(0, SK - allsims_anti[j, steps - 1]) + beta1 * cv;
                            }
                        }
                        else
                        {
                            if (call)
                            {
                                CT_2[j, 0] = Math.Max(0, allsims_anti[j, steps - 1] - SK);
                            }
                            else
                            {
                                CT_2[j, 0] = Math.Max(0, SK - allsims_anti[j, steps - 1]);
                            }
                        }

                    }
                }
                // Combine the two payoff matrices as instruced in the antithetic variance reduction method.
                if (anti)
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = 0.5 * CT_1[i, 0] + 0.5 * CT_2[i, 0];
                    }
                }
                else
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = CT_1[i, 0];
                    }
                }
            };

            if (multithread)
            {
                // Implementation with multithreading.
                int count = 0;
                List<Thread> ThreadList = new List<Thread>();
                for (int i = 0; i < cores; i++)
                {
                    Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                    ThreadList.Add(t);
                    t.Start(count);
                    count = count + percore;
                }
                foreach (Thread t in ThreadList)
                {
                    t.Join();
                }
                foreach (Thread t in ThreadList)
                {
                    t.Abort();
                }
            }
            else
            {
                //No multithreading situation.
                percore = trials;
                Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                t.Start(0);
                t.Join();
                t.Abort();
            }
            return CT;

        }


        // Method that return the payoff matrix of given asian option.
        public double[,] getPayoff_asian()
        {

            double[,] CT = new double[trials, 1];
            double[,] CT_1 = new double[trials, 1];
            double[,] CT_2 = new double[trials, 1];
            double[,] mean_1 = new double[trials, 1];
            double[,] mean_2 = new double[trials, 1];
            double dt = 1.0 * T / (steps - 1);
            double nudt = (r - 0.5 * vol * vol) * dt;
            double sigsdt = vol * Math.Sqrt(dt);
            double erddt = Math.Exp(r * dt);

            // Calculate how many rows need to be handled by each processor (Multi threading)
            int percore;
            if (trials % cores != 0)
            {
                percore = (trials + (cores - trials % cores)) / cores;
            }
            else { percore = trials / cores; }


            Action<object> Payoff = x =>
            {
                long para = Convert.ToInt32(x);
                long start = para;
                long ending = para + percore;
                if (ending > trials) ending = trials;

                // Get the first payoff matrix CT_1 using random_mat_1 random matrix.
                for (long j = start; j < ending; j++)
                {
                    double cv = 0;
                    double[] single_trial_mean = new double[steps];
                    single_trial_mean[0] = allsims[j, 0];
                    for (int i = 1; i < steps; i++)
                    {
                        double t = (i - 1) * dt;
                        double d1 = (Math.Log(allsims[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                        double delta = 0;
                        if (call)
                        {
                            delta = CumDensity(d1);
                        }
                        else { delta = CumDensity(d1) - 1; }
                        cv = cv + delta * (allsims[j, i] - allsims[j, i - 1] * erddt);
                        single_trial_mean[i] = allsims[j, i];
                    }
                    mean_1[j, 0] = getMean(single_trial_mean);
                    if (control)
                    {
                        if (call)
                        {
                            CT_1[j, 0] = Math.Max(0, mean_1[j, 0] - SK) + beta1 * cv;
                        }
                        else
                        {
                            CT_1[j, 0] = Math.Max(0, SK - mean_1[j, 0]) + beta1 * cv;
                        }
                    }
                    else
                    {
                        if (call)
                        {
                            CT_1[j, 0] = Math.Max(0, mean_1[j, 0] - SK);
                        }
                        else
                        {
                            CT_1[j, 0] = Math.Max(0, SK - mean_1[j, 0]);
                        }
                    }

                }


                // Get the second payoff matrix CT_2 using random_mat_2 random matrix. Random_mat_2 has negative elements compared to random_mat_1.
                if (anti)
                {
                    for (long j = start; j < ending; j++)
                    {
                        double cv = 0;
                        double[] single_trial_mean = new double[steps];
                        single_trial_mean[0] = allsims_anti[j, 0];
                        for (int i = 1; i < steps; i++)
                        {
                            double t = (i - 1) * dt;
                            double d1 = (Math.Log(allsims_anti[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                            double delta = 0;
                            if (call)
                            {
                                delta = CumDensity(d1);
                            }
                            else { delta = CumDensity(d1) - 1; }
                            cv = cv + delta * (allsims_anti[j, i] - allsims_anti[j, i - 1] * erddt);
                            single_trial_mean[i] = allsims_anti[j, i];
                        }
                        mean_2[j, 0] = getMean(single_trial_mean);
                        if (control)
                        {
                            if (call)
                            {
                                CT_2[j, 0] = Math.Max(0, mean_2[j, 0] - SK) + beta1 * cv;
                            }
                            else
                            {
                                CT_2[j, 0] = Math.Max(0, SK - mean_2[j, 0]) + beta1 * cv;
                            }
                        }
                        else
                        {
                            if (call)
                            {
                                CT_2[j, 0] = Math.Max(0, mean_2[j, 0] - SK);
                            }
                            else
                            {
                                CT_2[j, 0] = Math.Max(0, SK - mean_2[j, 0]);
                            }
                        }

                    }
                }
                // Combine the two payoff matrices as instruced in the antithetic variance reduction method.
                if (anti)
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = 0.5 * CT_1[i, 0] + 0.5 * CT_2[i, 0];
                    }
                }
                else
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = CT_1[i, 0];
                    }
                }
            };

            if (multithread)
            {
                // Implementation with multithreading.
                int count = 0;
                List<Thread> ThreadList = new List<Thread>();
                for (int i = 0; i < cores; i++)
                {
                    Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                    ThreadList.Add(t);
                    t.Start(count);
                    count = count + percore;
                }
                foreach (Thread t in ThreadList)
                {
                    t.Join();
                }
                foreach (Thread t in ThreadList)
                {
                    t.Abort();
                }
            }
            else
            {
                //No multithreading situation.
                percore = trials;
                Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                t.Start(0);
                t.Join();
                t.Abort();
            }
            return CT;

        }

        // Method that return the payoff matrix of given barrier option.
        public double[,] getPayoff_barrier()
        {

            double[,] CT = new double[trials, 1];
            double[,] CT_1 = new double[trials, 1];
            double[,] CT_2 = new double[trials, 1];
            double[,] max_1 = new double[trials, 1];
            double[,] max_2 = new double[trials, 1];
            double[,] min_1 = new double[trials, 1];
            double[,] min_2 = new double[trials, 1];
            double dt = 1.0 * T / (steps - 1);
            double nudt = (r - 0.5 * vol * vol) * dt;
            double sigsdt = vol * Math.Sqrt(dt);
            double erddt = Math.Exp(r * dt);

            // Calculate how many rows need to be handled by each processor (Multi threading)
            int percore;
            if (trials % cores != 0)
            {
                percore = (trials + (cores - trials % cores)) / cores;
            }
            else { percore = trials / cores; }


            Action<object> Payoff = x =>
            {
                long para = Convert.ToInt32(x);
                long start = para;
                long ending = para + percore;
                if (ending > trials) ending = trials;

                // Get the first payoff matrix CT_1 using random_mat_1 random matrix.
                for (long j = start; j < ending; j++)
                {
                    double cv = 0;
                    double[] single_trial_max = new double[steps];
                    double[] single_trial_min = new double[steps];
                    if (barrier_type <= 2)
                    {
                        single_trial_max[0] = allsims[j, 0];
                    }
                    else
                    {
                        single_trial_min[0] = allsims[j, 0];
                    }
                    for (int i = 1; i < steps; i++)
                    {
                        double t = (i - 1) * dt;
                        double d1 = (Math.Log(allsims[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                        double delta = 0;
                        if (call)
                        {
                            delta = CumDensity(d1);
                        }
                        else { delta = CumDensity(d1) - 1; }
                        cv = cv + delta * (allsims[j, i] - allsims[j, i - 1] * erddt);
                        if (barrier_type <= 2)
                        {
                            single_trial_max[i] = allsims[j, i];
                        }
                        else
                        {
                            single_trial_min[i] = allsims[j, i];
                        }
                    }
                    if (barrier_type == 1)
                    {
                        max_1[j, 0] = getMax(single_trial_max);
                        if (max_1[j, 0] <= barrier_price)
                        {
                            max_1[j, 0] = SK;
                        }
                        else
                        {
                            max_1[j, 0] = allsims[j, steps - 1];
                        }
                    }
                    if (barrier_type == 2)
                    {
                        max_1[j, 0] = getMax(single_trial_max);
                        if (max_1[j, 0] > barrier_price)
                        {
                            max_1[j, 0] = SK;
                        }
                        else
                        {
                            max_1[j, 0] = allsims[j, steps - 1];
                        }
                    }
                    if (barrier_type == 3)
                    {
                        min_1[j, 0] = getMin(single_trial_min);
                        if (min_1[j, 0] >= barrier_price)
                        {
                            min_1[j, 0] = SK;
                        }
                        else
                        {
                            min_1[j, 0] = allsims[j, steps - 1];
                        }
                    }
                    if (barrier_type == 4)
                    {
                        min_1[j, 0] = getMin(single_trial_min);
                        if (min_1[j, 0] < barrier_price)
                        {
                            min_1[j, 0] = SK;
                        }
                        else
                        {
                            min_1[j, 0] = allsims[j, steps - 1];
                        }
                    }
                    if (control)
                    {
                        if (call)
                        {
                            if (barrier_type == 1)
                            {
                                CT_1[j, 0] = Math.Max(0, max_1[j, 0] - SK) + beta1 * cv;
                            }
                            if (barrier_type == 2)
                            {
                                CT_1[j, 0] = Math.Max(0, max_1[j, 0] - SK) + beta1 * cv;
                            }
                            if (barrier_type == 3)
                            {
                                CT_1[j, 0] = Math.Max(0, min_1[j, 0] - SK) + beta1 * cv;
                            }
                            if (barrier_type == 4)
                            {
                                CT_1[j, 0] = Math.Max(0, min_1[j, 0] - SK) + beta1 * cv;
                            }

                        }
                        else
                        {
                            if (barrier_type == 1)
                            {
                                CT_1[j, 0] = Math.Max(0, SK - max_1[j, 0]) + beta1 * cv;
                            }
                            if (barrier_type == 2)
                            {
                                CT_1[j, 0] = Math.Max(0, SK - max_1[j, 0]) + beta1 * cv;
                            }
                            if (barrier_type == 3)
                            {
                                CT_1[j, 0] = Math.Max(0, SK - min_1[j, 0]) + beta1 * cv;
                            }
                            if (barrier_type == 4)
                            {
                                CT_1[j, 0] = Math.Max(0, SK - min_1[j, 0]) + beta1 * cv;
                            }
                        }
                    }
                    else
                    {
                        if (call)
                        {
                            if (barrier_type == 1)
                            {
                                CT_1[j, 0] = Math.Max(0, max_1[j, 0] - SK);
                            }
                            if (barrier_type == 2)
                            {
                                CT_1[j, 0] = Math.Max(0, max_1[j, 0] - SK);
                            }
                            if (barrier_type == 3)
                            {
                                CT_1[j, 0] = Math.Max(0, min_1[j, 0] - SK);
                            }
                            if (barrier_type == 4)
                            {
                                CT_1[j, 0] = Math.Max(0, min_1[j, 0] - SK);
                            }
                        }
                        else
                        {
                            if (barrier_type == 1)
                            {
                                CT_1[j, 0] = Math.Max(0, SK - max_1[j, 0]);
                            }
                            if (barrier_type == 2)
                            {
                                CT_1[j, 0] = Math.Max(0, SK - max_1[j, 0]);
                            }
                            if (barrier_type == 3)
                            {
                                CT_1[j, 0] = Math.Max(0, SK - min_1[j, 0]);
                            }
                            if (barrier_type == 4)
                            {
                                CT_1[j, 0] = Math.Max(0, SK - min_1[j, 0]);
                            }
                        }
                    }

                }


                // Get the second payoff matrix CT_2 using random_mat_2 random matrix. Random_mat_2 has negative elements compared to random_mat_1.
                if (anti)
                {
                    for (long j = start; j < ending; j++)
                    {
                        double cv = 0;
                        double[] single_trial_max = new double[steps];
                        double[] single_trial_min = new double[steps];
                        if (barrier_type <= 2)
                        {
                            single_trial_max[0] = allsims_anti[j, 0];
                        }
                        else
                        {
                            single_trial_min[0] = allsims_anti[j, 0];
                        }
                        for (int i = 1; i < steps; i++)
                        {
                            double t = (i - 1) * dt;
                            double d1 = (Math.Log(allsims_anti[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                            double delta = 0;
                            if (call)
                            {
                                delta = CumDensity(d1);
                            }
                            else { delta = CumDensity(d1) - 1; }
                            cv = cv + delta * (allsims_anti[j, i] - allsims_anti[j, i - 1] * erddt);
                            if (barrier_type <= 2)
                            {
                                single_trial_max[i] = allsims_anti[j, i];
                            }
                            else
                            {
                                single_trial_min[i] = allsims_anti[j, i];
                            }
                        }

                        if (barrier_type == 1)
                        {
                            max_2[j, 0] = getMax(single_trial_max);
                            if (max_2[j, 0] <= barrier_price)
                            {
                                max_2[j, 0] = SK;
                            }
                            else
                            {
                                max_2[j, 0] = allsims_anti[j, steps - 1];
                            }
                        }
                        if (barrier_type == 2)
                        {
                            max_2[j, 0] = getMax(single_trial_max);
                            if (max_2[j, 0] > barrier_price)
                            {
                                max_2[j, 0] = SK;
                            }
                            else
                            {
                                max_2[j, 0] = allsims_anti[j, steps - 1];
                            }
                        }
                        if (barrier_type == 3)
                        {
                            min_2[j, 0] = getMin(single_trial_min);
                            if (min_2[j, 0] >= barrier_price)
                            {
                                min_2[j, 0] = SK;
                            }
                            else
                            {
                                min_2[j, 0] = allsims_anti[j, steps - 1];
                            }
                        }
                        if (barrier_type == 4)
                        {
                            min_2[j, 0] = getMin(single_trial_min);
                            if (min_2[j, 0] < barrier_price)
                            {
                                min_2[j, 0] = SK;
                            }
                            else
                            {
                                min_2[j, 0] = allsims_anti[j, steps - 1];
                            }
                        }

                        if (control)
                        {
                            if (call)
                            {
                                if (barrier_type == 1)
                                {
                                    CT_2[j, 0] = Math.Max(0, max_2[j, 0] - SK) + beta1 * cv;
                                }
                                if (barrier_type == 2)
                                {
                                    CT_2[j, 0] = Math.Max(0, max_2[j, 0] - SK) + beta1 * cv;
                                }
                                if (barrier_type == 3)
                                {
                                    CT_2[j, 0] = Math.Max(0, min_2[j, 0] - SK) + beta1 * cv;
                                }
                                if (barrier_type == 4)
                                {
                                    CT_2[j, 0] = Math.Max(0, min_2[j, 0] - SK) + beta1 * cv;
                                }
                            }
                            else
                            {
                                if (barrier_type == 1)
                                {
                                    CT_2[j, 0] = Math.Max(0, SK - max_2[j, 0]) + beta1 * cv;
                                }
                                if (barrier_type == 2)
                                {
                                    CT_2[j, 0] = Math.Max(0, SK - max_2[j, 0]) + beta1 * cv;
                                }
                                if (barrier_type == 3)
                                {
                                    CT_2[j, 0] = Math.Max(0, SK - min_2[j, 0]) + beta1 * cv;
                                }
                                if (barrier_type == 4)
                                {
                                    CT_2[j, 0] = Math.Max(0, SK - min_2[j, 0]) + beta1 * cv;
                                }
                            }
                        }
                        else
                        {
                            if (call)
                            {
                                if (barrier_type == 1)
                                {
                                    CT_2[j, 0] = Math.Max(0, max_2[j, 0] - SK);
                                }
                                if (barrier_type == 2)
                                {
                                    CT_2[j, 0] = Math.Max(0, max_2[j, 0] - SK);
                                }
                                if (barrier_type == 3)
                                {
                                    CT_2[j, 0] = Math.Max(0, min_2[j, 0] - SK);
                                }
                                if (barrier_type == 4)
                                {
                                    CT_2[j, 0] = Math.Max(0, min_2[j, 0] - SK);
                                }
                            }
                            else
                            {
                                if (barrier_type == 1)
                                {
                                    CT_2[j, 0] = Math.Max(0, SK - max_2[j, 0]);
                                }
                                if (barrier_type == 2)
                                {
                                    CT_2[j, 0] = Math.Max(0, SK - max_2[j, 0]);
                                }
                                if (barrier_type == 3)
                                {
                                    CT_2[j, 0] = Math.Max(0, SK - min_2[j, 0]);
                                }
                                if (barrier_type == 4)
                                {
                                    CT_2[j, 0] = Math.Max(0, SK - min_2[j, 0]);
                                }
                            }
                        }

                    }
                }
                // Combine the two payoff matrices as instruced in the antithetic variance reduction method.
                if (anti)
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = 0.5 * CT_1[i, 0] + 0.5 * CT_2[i, 0];
                    }
                }
                else
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = CT_1[i, 0];
                    }
                }
            };

            if (multithread)
            {
                // Implementation with multithreading.
                int count = 0;
                List<Thread> ThreadList = new List<Thread>();
                for (int i = 0; i < cores; i++)
                {
                    Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                    ThreadList.Add(t);
                    t.Start(count);
                    count = count + percore;
                }
                foreach (Thread t in ThreadList)
                {
                    t.Join();
                }
                foreach (Thread t in ThreadList)
                {
                    t.Abort();
                }
            }
            else
            {
                //No multithreading situation.
                percore = trials;
                Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                t.Start(0);
                t.Join();
                t.Abort();
            }
            return CT;

        }


        // Method that return the payoff matrix of given lookback option.
        public double[,] getPayoff_lookback()
        {

            double[,] CT = new double[trials, 1];
            double[,] CT_1 = new double[trials, 1];
            double[,] CT_2 = new double[trials, 1];
            double[,] max_1 = new double[trials, 1];
            double[,] max_2 = new double[trials, 1];
            double[,] min_1 = new double[trials, 1];
            double[,] min_2 = new double[trials, 1];
            double dt = 1.0 * T / (steps - 1);
            double nudt = (r - 0.5 * vol * vol) * dt;
            double sigsdt = vol * Math.Sqrt(dt);
            double erddt = Math.Exp(r * dt);

            // Calculate how many rows need to be handled by each processor (Multi threading)
            int percore;
            if (trials % cores != 0)
            {
                percore = (trials + (cores - trials % cores)) / cores;
            }
            else { percore = trials / cores; }


            Action<object> Payoff = x =>
            {
                long para = Convert.ToInt32(x);
                long start = para;
                long ending = para + percore;
                if (ending > trials) ending = trials;

                // Get the first payoff matrix CT_1 using random_mat_1 random matrix.
                for (long j = start; j < ending; j++)
                {
                    double cv = 0;
                    double[] single_trial_max = new double[steps];
                    double[] single_trial_min = new double[steps];
                    if (call)
                    {
                        single_trial_max[0] = allsims[j, 0];
                    }
                    else
                    {
                        single_trial_min[0] = allsims[j, 0];
                    }
                    for (int i = 1; i < steps; i++)
                    {
                        double t = (i - 1) * dt;
                        double d1 = (Math.Log(allsims[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                        double delta = 0;
                        if (call)
                        {
                            delta = CumDensity(d1);
                        }
                        else { delta = CumDensity(d1) - 1; }
                        cv = cv + delta * (allsims[j, i] - allsims[j, i - 1] * erddt);
                        if (call)
                        {
                            single_trial_max[i] = allsims[j, i];
                        }
                        else
                        {
                            single_trial_min[i] = allsims[j, i];
                        }
                    }
                    if (call)
                    {
                        max_1[j, 0] = getMax(single_trial_max);
                    }
                    else
                    {
                        min_1[j, 0] = getMin(single_trial_min);
                    }
                    if (control)
                    {
                        if (call)
                        {
                            CT_1[j, 0] = Math.Max(0, max_1[j, 0] - SK) + beta1 * cv;
                        }
                        else
                        {
                            CT_1[j, 0] = Math.Max(0, SK - min_1[j, 0]) + beta1 * cv;
                        }
                    }
                    else
                    {
                        if (call)
                        {
                            CT_1[j, 0] = Math.Max(0, max_1[j, 0] - SK);
                        }
                        else
                        {
                            CT_1[j, 0] = Math.Max(0, SK - min_1[j, 0]);
                        }
                    }

                }


                // Get the second payoff matrix CT_2 using random_mat_2 random matrix. Random_mat_2 has negative elements compared to random_mat_1.
                if (anti)
                {
                    for (long j = start; j < ending; j++)
                    {
                        double cv = 0;
                        double[] single_trial_max = new double[steps];
                        double[] single_trial_min = new double[steps];
                        if (call)
                        {
                            single_trial_max[0] = allsims_anti[j, 0];
                        }
                        else
                        {
                            single_trial_min[0] = allsims_anti[j, 0];
                        }

                        for (int i = 1; i < steps; i++)
                        {
                            double t = (i - 1) * dt;
                            double d1 = (Math.Log(allsims_anti[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                            double delta = 0;
                            if (call)
                            {
                                delta = CumDensity(d1);
                            }
                            else { delta = CumDensity(d1) - 1; }
                            cv = cv + delta * (allsims_anti[j, i] - allsims_anti[j, i - 1] * erddt);
                            if (call)
                            {
                                single_trial_max[i] = allsims_anti[j, i];
                            }
                            else
                            {
                                single_trial_min[i] = allsims_anti[j, i];
                            }
                        }
                        if (call)
                        {
                            max_2[j, 0] = getMax(single_trial_max);
                        }
                        else
                        {
                            min_2[j, 0] = getMin(single_trial_min);
                        }
                        if (control)
                        {
                            if (call)
                            {
                                CT_2[j, 0] = Math.Max(0, max_2[j, 0] - SK) + beta1 * cv;
                            }
                            else
                            {
                                CT_2[j, 0] = Math.Max(0, SK - min_2[j, 0]) + beta1 * cv;
                            }
                        }
                        else
                        {
                            if (call)
                            {
                                CT_2[j, 0] = Math.Max(0, max_2[j, 0] - SK);
                            }
                            else
                            {
                                CT_2[j, 0] = Math.Max(0, SK - min_2[j, 0]);
                            }
                        }

                    }
                }
                // Combine the two payoff matrices as instruced in the antithetic variance reduction method.
                if (anti)
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = 0.5 * CT_1[i, 0] + 0.5 * CT_2[i, 0];
                    }
                }
                else
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = CT_1[i, 0];
                    }
                }
            };

            if (multithread)
            {
                // Implementation with multithreading.
                int count = 0;
                List<Thread> ThreadList = new List<Thread>();
                for (int i = 0; i < cores; i++)
                {
                    Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                    ThreadList.Add(t);
                    t.Start(count);
                    count = count + percore;
                }
                foreach (Thread t in ThreadList)
                {
                    t.Join();
                }
                foreach (Thread t in ThreadList)
                {
                    t.Abort();
                }
            }
            else
            {
                //No multithreading situation.
                percore = trials;
                Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                t.Start(0);
                t.Join();
                t.Abort();
            }
            return CT;

        }

        // Method that return the payoff matrix of given digital option.
        public double[,] getPayoff_digital()
        {

            double[,] CT = new double[trials, 1];
            double[,] CT_1 = new double[trials, 1];
            double[,] CT_2 = new double[trials, 1];
            double dt = 1.0 * T / (steps - 1);
            double nudt = (r - 0.5 * vol * vol) * dt;
            double sigsdt = vol * Math.Sqrt(dt);
            double erddt = Math.Exp(r * dt);

            // Calculate how many rows need to be handled by each processor (Multi threading)
            int percore;
            if (trials % cores != 0)
            {
                percore = (trials + (cores - trials % cores)) / cores;
            }
            else { percore = trials / cores; }


            Action<object> Payoff = x =>
            {
                long para = Convert.ToInt32(x);
                long start = para;
                long ending = para + percore;
                if (ending > trials) ending = trials;

                // Get the first payoff matrix CT_1 using random_mat_1 random matrix.
                for (long j = start; j < ending; j++)
                {
                    double cv = 0;
                    for (int i = 1; i < steps; i++)
                    {
                        double t = (i - 1) * dt;
                        double d1 = (Math.Log(allsims[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                        double delta = 0;
                        if (call)
                        {
                            delta = CumDensity(d1);
                        }
                        else { delta = CumDensity(d1) - 1; }
                        cv = cv + delta * (allsims[j, i] - allsims[j, i - 1] * erddt);
                    }
                    if (control)
                    {
                        if (call)
                        {
                            if (allsims[j, steps - 1] - SK > 0)
                            {
                                CT_1[j, 0] = digital_rebate + beta1 * cv;
                            }
                            else
                            {
                                CT_1[j, 0] = beta1 * cv;
                            }
                        }
                        else
                        {
                            if (SK - allsims[j, steps - 1] > 0)
                            {
                                CT_1[j, 0] = digital_rebate + beta1 * cv;
                            }
                            else
                            {
                                CT_1[j, 0] = beta1 * cv;
                            }
                        }
                    }
                    else
                    {
                        if (call)
                        {
                            if (allsims[j, steps - 1] - SK > 0)
                            {
                                CT_1[j, 0] = digital_rebate;
                            }
                            else
                            {
                                CT_1[j, 0] = 0;
                            }
                        }
                        else
                        {
                            if (SK - allsims[j, steps - 1] > 0)
                            {
                                CT_1[j, 0] = digital_rebate;
                            }
                            else
                            {
                                CT_1[j, 0] = 0;
                            }
                        }
                    }

                }


                // Get the second payoff matrix CT_2 using random_mat_2 random matrix. Random_mat_2 has negative elements compared to random_mat_1.
                if (anti)
                {
                    for (long j = start; j < ending; j++)
                    {
                        double cv = 0;
                        for (int i = 1; i < steps; i++)
                        {
                            double t = (i - 1) * dt;
                            double d1 = (Math.Log(allsims_anti[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                            double delta = 0;
                            if (call)
                            {
                                delta = CumDensity(d1);
                            }
                            else { delta = CumDensity(d1) - 1; }
                            cv = cv + delta * (allsims_anti[j, i] - allsims_anti[j, i - 1] * erddt);
                        }
                        if (control)
                        {
                            if (call)
                            {
                                if (allsims_anti[j, steps - 1] - SK > 0)
                                {
                                    CT_2[j, 0] = digital_rebate + beta1 * cv;
                                }
                                else
                                {
                                    CT_2[j, 0] = beta1 * cv;
                                }
                            }
                            else
                            {
                                if (SK - allsims_anti[j, steps - 1] > 0)
                                {
                                    CT_2[j, 0] = digital_rebate + beta1 * cv;
                                }
                                else
                                {
                                    CT_2[j, 0] = beta1 * cv;
                                }
                            }
                        }
                        else
                        {
                            if (call)
                            {
                                if (allsims_anti[j, steps - 1] - SK > 0)
                                {
                                    CT_2[j, 0] = digital_rebate;
                                }
                                else
                                {
                                    CT_2[j, 0] = 0;
                                }
                            }
                            else
                            {
                                if (SK - allsims_anti[j, steps - 1] > 0)
                                {
                                    CT_2[j, 0] = digital_rebate;
                                }
                                else
                                {
                                    CT_2[j, 0] = 0;
                                }
                            }
                        }

                    }
                }
                // Combine the two payoff matrices as instruced in the antithetic variance reduction method.
                if (anti)
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = 0.5 * CT_1[i, 0] + 0.5 * CT_2[i, 0];
                    }
                }
                else
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = CT_1[i, 0];
                    }
                }
            };

            if (multithread)
            {
                // Implementation with multithreading.
                int count = 0;
                List<Thread> ThreadList = new List<Thread>();
                for (int i = 0; i < cores; i++)
                {
                    Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                    ThreadList.Add(t);
                    t.Start(count);
                    count = count + percore;
                }
                foreach (Thread t in ThreadList)
                {
                    t.Join();
                }
                foreach (Thread t in ThreadList)
                {
                    t.Abort();
                }
            }
            else
            {
                //No multithreading situation.
                percore = trials;
                Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                t.Start(0);
                t.Join();
                t.Abort();
            }
            return CT;

        }

        // Method that return the payoff matrix of given range option.
        public double[,] getPayoff_range()
        {

            double[,] CT = new double[trials, 1];
            double[,] CT_1 = new double[trials, 1];
            double[,] CT_2 = new double[trials, 1];
            double[,] max_1 = new double[trials, 1];
            double[,] max_2 = new double[trials, 1];
            double[,] min_1 = new double[trials, 1];
            double[,] min_2 = new double[trials, 1];
            double dt = 1.0 * T / (steps - 1);
            double nudt = (r - 0.5 * vol * vol) * dt;
            double sigsdt = vol * Math.Sqrt(dt);
            double erddt = Math.Exp(r * dt);

            // Calculate how many rows need to be handled by each processor (Multi threading)
            int percore;
            if (trials % cores != 0)
            {
                percore = (trials + (cores - trials % cores)) / cores;
            }
            else { percore = trials / cores; }


            Action<object> Payoff = x =>
            {
                long para = Convert.ToInt32(x);
                long start = para;
                long ending = para + percore;
                if (ending > trials) ending = trials;

                // Get the first payoff matrix CT_1 using random_mat_1 random matrix.
                for (long j = start; j < ending; j++)
                {
                    double cv = 0;
                    double[] single_trial_max = new double[steps];
                    double[] single_trial_min = new double[steps];
                    single_trial_max[0] = allsims[j, 0];
                    single_trial_min[0] = allsims[j, 0];
                    for (int i = 1; i < steps; i++)
                    {
                        double t = (i - 1) * dt;
                        double d1 = (Math.Log(allsims[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                        double delta = 0;
                        if (call)
                        {
                            delta = CumDensity(d1);
                        }
                        else { delta = CumDensity(d1) - 1; }
                        cv = cv + delta * (allsims[j, i] - allsims[j, i - 1] * erddt);
                        single_trial_max[i] = allsims[j, i];
                        single_trial_min[i] = allsims[j, i];
                    }
                    max_1[j, 0] = getMax(single_trial_max);
                    min_1[j, 0] = getMin(single_trial_max);
                    if (control)
                    {
                        CT_1[j, 0] = max_1[j, 0] - min_1[j, 0] + beta1 * cv;
                    }
                    else
                    {
                        CT_1[j, 0] = max_1[j, 0] - min_1[j, 0];
                    }

                }


                // Get the second payoff matrix CT_2 using random_mat_2 random matrix. Random_mat_2 has negative elements compared to random_mat_1.
                if (anti)
                {
                    for (long j = start; j < ending; j++)
                    {
                        double cv = 0;
                        double[] single_trial_max = new double[steps];
                        double[] single_trial_min = new double[steps];
                        single_trial_max[0] = allsims_anti[j, 0];
                        single_trial_min[0] = allsims_anti[j, 0];
                        for (int i = 1; i < steps; i++)
                        {
                            double t = (i - 1) * dt;
                            double d1 = (Math.Log(allsims_anti[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
                            double delta = 0;
                            if (call)
                            {
                                delta = CumDensity(d1);
                            }
                            else { delta = CumDensity(d1) - 1; }
                            cv = cv + delta * (allsims_anti[j, i] - allsims_anti[j, i - 1] * erddt);
                            single_trial_max[i] = allsims_anti[j, i];
                            single_trial_min[i] = allsims_anti[j, i];
                        }
                        max_2[j, 0] = getMax(single_trial_max);
                        min_2[j, 0] = getMin(single_trial_max);
                        if (control)
                        {
                            CT_2[j, 0] = max_2[j, 0] - min_2[j, 0] + beta1 * cv;
                        }
                        else
                        {
                            CT_2[j, 0] = max_2[j, 0] - min_2[j, 0];
                        }

                    }
                }
                // Combine the two payoff matrices as instruced in the antithetic variance reduction method.
                if (anti)
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = 0.5 * CT_1[i, 0] + 0.5 * CT_2[i, 0];
                    }
                }
                else
                {
                    for (long i = start; i < ending; i++)
                    {
                        CT[i, 0] = CT_1[i, 0];
                    }
                }
            };

            if (multithread)
            {
                // Implementation with multithreading.
                int count = 0;
                List<Thread> ThreadList = new List<Thread>();
                for (int i = 0; i < cores; i++)
                {
                    Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                    ThreadList.Add(t);
                    t.Start(count);
                    count = count + percore;
                }
                foreach (Thread t in ThreadList)
                {
                    t.Join();
                }
                foreach (Thread t in ThreadList)
                {
                    t.Abort();
                }
            }
            else
            {
                //No multithreading situation.
                percore = trials;
                Thread t = new Thread(new ParameterizedThreadStart(Payoff));
                t.Start(0);
                t.Join();
                t.Abort();
            }
            return CT;

        }





        ////Stock matrix generated by a random number matrix. This method calculate the payoff matrix without antithetic variance reduction method.
        //public double[,] stockmat()
        //{

        //    double[,] allsims = new double[trials, steps];
        //    double[,] CT = new double[trials, 1];
        //    double[,] mean_1 = new double[trials, 1];
        //    double dt = 1.0 * T / (steps - 1);
        //    double nudt = (r - 0.5 * vol * vol) * dt;
        //    double sigsdt = vol * Math.Sqrt(dt);
        //    double erddt = Math.Exp(r * dt);

        //    // Calculate how many rows need to be handled by each processor (Multi threading)
        //    int percore;
        //    if (trials % cores != 0)
        //    {
        //        percore = (trials + (cores - trials % cores)) / cores;
        //    }
        //    else { percore = trials / cores; }
        //    // Anonymous function that can assign value to a block of matrix with the specification of start row and end row.
        //    Action<object> Payoff = x =>
        //    {
        //        long para = Convert.ToInt32(x);
        //        long start = para;
        //        long ending = para + percore;
        //        if (ending > trials) ending = trials;
        //        for (long i = start; i < ending; i++)
        //        {
        //            allsims[i, 0] = S0;
        //        }
        //        for (long j = start; j < ending; j++)
        //        {
        //            double cv = 0;
        //            for (int i = 1; i < steps; i++)
        //            {
        //                // The calculation of cv which will be used in Delta-based control variate method.
        //                double delta = 0;
        //                double t = (i - 1) * dt;
        //                // Calculate 'd1' in B-S model.
        //                double d1 = (Math.Log(allsims[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
        //                // Get the delta for this option.
        //                if (call)
        //                {
        //                    delta = CumDensity(d1);
        //                }
        //                else { delta = CumDensity(d1) - 1; }
        //                allsims[j, i] = allsims[j, i - 1] * Math.Exp(nudt + sigsdt * Form1.random_mat[j, i - 1]);
        //                cv = cv + delta * (allsims[j, i] - allsims[j, i - 1] * erddt);
        //            }

        //            if (type == "asian") {
        //                double[] meanvect=new double[steps];
        //                for (int i = 0; i < steps; i++)
        //                {
        //                    meanvect[i] = allsims[j, i];
        //                }
        //                mean_1[j,0]= getMean(meanvect);
        //            }
        //            // Control variate method is used.
        //            if (control)
        //            {
        //                if (call)
        //                {
        //                    if (type == "euro") {
        //                        CT[j, 0] = Math.Max(0, allsims[j, steps - 1] - SK) + beta1 * cv;
        //                    } else if (type == "asian")
        //                    {
        //                        CT[j, 0] = Math.Max(0, mean_1[j, 0] - SK) + beta1 * cv;
        //                    }
        //                }
        //                else
        //                {
        //                    if (type == "euro")
        //                    {
        //                        CT[j, 0] = Math.Max(0, SK - allsims[j, steps - 1]) + beta1 * cv;
        //                    }
        //                    else if (type == "asian")
        //                    {
        //                        CT[j, 0] = Math.Max(0,SK- mean_1[j, 0] ) + beta1 * cv;
        //                    }
        //                }
        //            }
        //            // Without delta-based control variate.
        //            else
        //            {
        //                if (call)
        //                {
        //                    if (type == "euro")
        //                    {
        //                        CT[j, 0] = Math.Max(0, allsims[j, steps - 1] - SK);
        //                    }
        //                    else if (type == "asian")
        //                    {
        //                        CT[j, 0] = Math.Max(0, mean_1[j, 0] - SK);
        //                    }
        //                }
        //                else
        //                {
        //                    if (type == "euro")
        //                    {
        //                        CT[j, 0] = Math.Max(0, SK - allsims[j, steps - 1]);
        //                    }
        //                    else if (type == "asian")
        //                    {
        //                        CT[j, 0] = Math.Max(0, SK - mean_1[j, 0]);
        //                    }
        //                }
        //            }

        //        }

        //    };

        //    if (multithread)
        //    {
        //        // Implementation with multithreading.
        //        int count = 0;
        //        List<Thread> ThreadList = new List<Thread>();
        //        for (int i = 0; i < cores; i++)
        //        {
        //            Thread t = new Thread(new ParameterizedThreadStart(Payoff));
        //            ThreadList.Add(t);
        //            t.Start(count);
        //            count = count + percore;
        //        }
        //        foreach (Thread t in ThreadList)
        //        {
        //            t.Join();
        //        }
        //        foreach (Thread t in ThreadList)
        //        {
        //            t.Abort();
        //        }
        //    }
        //    else
        //    {
        //        //No multithreading situation.
        //        percore = trials;
        //        Thread t = new Thread(new ParameterizedThreadStart(Payoff));
        //        t.Start(0);
        //        t.Join();
        //        t.Abort();
        //    }
        //    return CT;
        //}


        //// Stock matrix generated by a random number matrix. This method calculate the payoff matrix with antithetic variance reduction method.
        //// Almost the same thing as stockmat() method except that this time we will have two payoff matrix and then we sum them up and divide it by two.
        //// (Antithetic variance reduction method).
        //public double[,] stockmat_Anti()
        //{
        //    double[,] allsims = new double[trials, steps];
        //    double[,] allsims_anti = new double[trials, steps];
        //    double[,] CT = new double[trials, 1];
        //    double[,] CT_1 = new double[trials, 1];
        //    double[,] CT_2 = new double[trials, 1];
        //    double[,] mean_1 = new double[trials, 1];
        //    double[,] mean_2 = new double[trials, 1];
        //    double dt = 1.0 * T / (steps - 1);
        //    double nudt = (r - 0.5 * vol * vol) * dt;
        //    double sigsdt = vol * Math.Sqrt(dt);
        //    double erddt = Math.Exp(r * dt);

        //    // Calculate how many rows need to be handled by each processor (Multi threading)
        //    int percore;
        //    if (trials % cores != 0)
        //    {
        //        percore = (trials + (cores - trials % cores)) / cores;
        //    }
        //    else { percore = trials / cores; }
        //    // Anonymous function that can assign value to a block of matrix with the specification of start row and end row.
        //    Action<object> Payoff_anti = x =>
        //    {
        //        long para = Convert.ToInt32(x);
        //        long start = para;
        //        long ending = para + percore;
        //        if (ending > trials) ending = trials;
        //        for (long i = start; i < ending; i++)
        //        {
        //            allsims[i, 0] = S0;
        //            allsims_anti[i, 0] = S0;
        //        }

        //        // Get the first payoff matrix CT_1 using random_mat_1 random matrix.
        //        for (long j = start; j < ending; j++)
        //        {
        //            double cv = 0;
        //            for (int i = 1; i < steps; i++)
        //            {
        //                double t = (i - 1) * dt;
        //                double d1 = (Math.Log(allsims[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
        //                double delta = 0;
        //                if (call)
        //                {
        //                    delta = CumDensity(d1);
        //                }
        //                else { delta = CumDensity(d1) - 1; }
        //                allsims[j, i] = allsims[j, i - 1] * Math.Exp(nudt + sigsdt * Form1.random_mat[j, i - 1]);
        //                cv = cv + delta * (allsims[j, i] - allsims[j, i - 1] * erddt);
        //            }
        //            if (type == "asian")
        //            {
        //                double[] meanvect = new double[steps];
        //                for (int i = 0; i < steps; i++)
        //                {
        //                    meanvect[i] = allsims[j, i];
        //                }
        //                mean_1[j, 0] = getMean(meanvect);
        //            }
        //            if (control)
        //            {
        //                if (call)
        //                {
        //                    CT_1[j, 0] = Math.Max(0, allsims[j, steps - 1] - SK) + beta1 * cv;
        //                }
        //                else
        //                {
        //                    CT_1[j, 0] = Math.Max(0, SK - allsims[j, steps - 1]) + beta1 * cv;
        //                }
        //            }
        //            else
        //            {
        //                if (call)
        //                {
        //                    CT_1[j, 0] = Math.Max(0, allsims[j, steps - 1] - SK);
        //                }
        //                else
        //                {
        //                    CT_1[j, 0] = Math.Max(0, SK - allsims[j, steps - 1]);
        //                }
        //            }

        //        }


        //        // Get the second payoff matrix CT_2 using random_mat_2 random matrix. Random_mat_2 has negative elements compared to random_mat_1.
        //        for (long j = start; j < ending; j++)
        //        {
        //            double cv = 0;
        //            for (int i = 1; i < steps; i++)
        //            {
        //                double t = (i - 1) * dt;
        //                double d1 = (Math.Log(allsims_anti[j, i - 1] / SK) + (r + 0.5 * vol * vol) * (T - t)) / (vol * Math.Sqrt(T - t));
        //                double delta = 0;
        //                if (call)
        //                {
        //                    delta = CumDensity(d1);
        //                }
        //                else { delta = CumDensity(d1) - 1; }
        //                allsims_anti[j, i] = allsims_anti[j, i - 1] * Math.Exp(nudt + sigsdt * Form1.random_mat_1[j, i - 1]);
        //                cv = cv + delta * (allsims_anti[j, i] - allsims_anti[j, i - 1] * erddt);
        //            }
        //            if (control)
        //            {
        //                if (call)
        //                {
        //                    CT_2[j, 0] = Math.Max(0, allsims_anti[j, steps - 1] - SK) + beta1 * cv;
        //                }
        //                else
        //                {
        //                    CT_2[j, 0] = Math.Max(0, SK - allsims_anti[j, steps - 1]) + beta1 * cv;
        //                }
        //            }
        //            else
        //            {
        //                if (call)
        //                {
        //                    CT_2[j, 0] = Math.Max(0, allsims_anti[j, steps - 1] - SK);
        //                }
        //                else
        //                {
        //                    CT_2[j, 0] = Math.Max(0, SK - allsims_anti[j, steps - 1]);
        //                }
        //            }

        //        }

        //        // Combine the two payoff matrices as instruced in the antithetic variance reduction method.
        //        for (long i = start; i < ending; i++)
        //        {
        //            CT[i, 0] = 0.5 * CT_1[i, 0] + 0.5 * CT_2[i, 0];
        //        }
        //    };
        //    if (multithread)
        //    {
        //        // Multithreading implementation
        //        int count = 0;
        //        List<Thread> ThreadList = new List<Thread>();
        //        for (int i = 0; i < cores; i++)
        //        {
        //            Thread t = new Thread(new ParameterizedThreadStart(Payoff_anti));
        //            ThreadList.Add(t);
        //            t.Start(count);
        //            count = count + percore;
        //        }
        //        foreach (Thread t in ThreadList)
        //        {
        //            t.Join();
        //        }
        //        foreach (Thread t in ThreadList)
        //        {
        //            t.Abort();
        //        }
        //    }
        //    else
        //    {
        //        // No multithreading.
        //        percore = trials;
        //        Thread t = new Thread(new ParameterizedThreadStart(Payoff_anti));
        //        t.Start(0);
        //        t.Join();
        //        t.Abort();
        //    }
        //    return CT;
        //}


        // Method that can return the normcdf of an input double number. (Recall that in B-S model, delta=normcdf(d1) for Call option)
        static double getMean(double[] t)
        {
            double sum = 0;
            for (int i = 0; i < steps; i++)
            {
                sum = sum + t[i];
            }
            return sum / steps;
        }

        static double getMax(double[] t)
        {
            double max = t[0];
            for (int i = 0; i < steps; i++)
            {
                if (t[i] >= max)
                {
                    max = t[i];
                }
            }
            return max;
        }

        static double getMin(double[] t)
        {
            double min = t[0];
            for (int i = 0; i < steps; i++)
            {
                if (t[i] <= min)
                {
                    min = t[i];
                }
            }
            return min;
        }

        static double CumDensity(double z)
        {
            double p = 0.3275911;
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;

            int sign;
            if (z < 0.0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(z) / Math.Sqrt(2.0);
            double t = 1.0 / (1.0 + p * x);
            double erf = 1.0 - (((((a5 * t + a4) * t) + a3)
              * t + a2) * t + a1) * t * Math.Exp(-x * x);
            return 0.5 * (1.0 + sign * erf);
        }


    }
}
