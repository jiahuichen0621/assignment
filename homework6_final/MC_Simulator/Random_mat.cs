﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MC_Simulator
{
    class Random_mat
    {
        public int steps { get; set; }
        public int trials { get; set; }
        public static int cores = Environment.ProcessorCount;

        // Constructor which accepts the instance of Underlying class which is created in the main method.
        public Random_mat(Underlying under)
        {
            steps = under.steps;
            trials = under.trials;
        }

        // Box_muller method which returns a single random number.
        public static double box_muller(Random rdn1)
        {
            double x1, x2, z1, z2;
            double[] vect = new double[2];


            x1 = rdn1.NextDouble();
            x2 = rdn1.NextDouble();
            z1 = Math.Sqrt(-2 * Math.Log(x1)) * Math.Cos(2 * Math.PI * x2);
            z2 = Math.Sqrt(-2 * Math.Log(x1)) * Math.Sin(2 * Math.PI * x2);
            vect[0] = z1;
            vect[1] = z2;
            return vect[0];

        }

        // Random number matrix with row number equal to trials, column number equal to steps-1. Box_muller method is called many times within this method.
        public void r_mat()

        {
            Random rdn = new Random();

            for (int i = 0; i < trials; i++)
            {
                for (int j = 0; j < steps - 1; j++)
                {
                    // Access to the random_mat and random_mat_1 declared in the Form1 class directly and modify them directly.
                    Form1.random_mat[i, j] = box_muller(rdn);
                    Form1.random_mat_1[i, j] = -1 * Form1.random_mat[i, j];
                }
            }

        }


        public void multithread()
        {
            int percore;
            // Calculate how many rows need to be handled by each processor (Multi threading)
            if (trials % cores != 0)
            {
                percore = (trials + (cores - trials % cores)) / cores;
            }
            else { percore = trials / cores; }

            // Anonymous function that can assign value to a block of matrix with the specification of start row and end row.
            Action<object> myACt = x => {
                long para = Convert.ToInt32(x);
                // Set seed to the instance of random class
                Random rdn = new Random((int)DateTime.Now.Ticks & 0x0000FFFF + (int)para * 10 / percore);
                long start = para;
                long ending = para + percore;
                if (ending > trials) ending = trials; // in case of out of range error.
                for (long i = start; i < ending; i++)
                {
                    for (int j = 0; j < steps - 1; j++)
                    {
                        Form1.random_mat[i, j] = box_muller(rdn);
                        Form1.random_mat_1[i, j] = -1 * Form1.random_mat[i, j];
                    }
                }
            };

            int count = 0;
            List<Thread> ThreadList = new List<Thread>();
            // Open several threads to assign value to the random matrix at the same time. The number of threads should be equal to the number of processors on the machine.
            for (int i = 0; i < cores; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(myACt));
                ThreadList.Add(t);
                t.Start(count);
                count = count + percore;
            }
            foreach (Thread t in ThreadList)
            {
                t.Join();
            }
            foreach (Thread t in ThreadList)
            {
                t.Abort();
            }
        }

    }
}

