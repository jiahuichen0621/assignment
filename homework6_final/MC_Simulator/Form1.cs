﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MC_Simulator
{
    public partial class Form1 : Form
    {
        // Initialize the random matrix as static so they can be accessed in other class throught Form1.random_mat;
        public static double[,] random_mat;
        public static double[,] random_mat_1;
        public static int cores = Environment.ProcessorCount;
        Thread t;


        public Form1()
        {
            InitializeComponent();


        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Threading.ThreadState g = t.ThreadState;
            if (g == System.Threading.ThreadState.Running)
            {
                this.Text = "Can not close while thread running";
                e.Cancel = true;
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {
            //When user clicks the button "Calculate", initiate a new thread and start the thread (the calculation part). By doing so we can ensure that
            // the interface will not freeze when the calculation is ongoing because they are in different threads.
            // t = new Thread(new ThreadStart(Calc));
            //t.Start();

        }

        //Calculation part.
        public double[] Calc(string OptionType, bool iscall, double S0, double SK, double r, double vol, double T, int BarrierType, double barrier_price, double rebate)
        {
            // Initiate variables that would be used in the following code.
            double price = 0;
            double delta = 0;
            double vega = 0;
            double gamma = 0;
            double theta = 0;
            double rho = 0;
            double[] param = new double[7];
            int steps = 100;
            int trials = 10000;
            param[0] = S0;
            param[1] = SK;
            param[2] = r;

            param[3] = vol;
            param[4] = T;
            param[5] = steps;
            param[6] = trials;

            try
            {
                random_mat = new double[trials, steps - 1];
                random_mat_1 = new double[trials, steps - 1];
                // Initiate an instance of Underlying class using the user input.
                Underlying underlying = new Underlying(param);

                // Use this underlying to create an instance of Random_mat class, which contains a method that can generate a random matrix.
                Random_mat mat_generator = new Random_mat(underlying);
                // Call the r_mat() or multithread() method in Random_mat class to generate a random matrix and use this matrix in all the calculations of price and greeks.

                // If user uses multithreading, run multithread() method to modify the random_mat and random_mat_1 (used for antithetic).
                mat_generator.multithread();


                // Make sure all the inputs are valid (no less than zero, steps must be at least two).
                if ((param[0] >= 0) & (param[1] >= 0) & (param[2] >= 0) & (param[3] >= 0) & (param[4] >= 0) & (steps >= 2) & (trials > 0))
                {
                    // Set the value for variables iscall and control to pass into the following methods.

                    bool control = false;
                    bool anti = true;
                    bool multithread = true;

                    //European Option Implementation.
                    if (OptionType.ToUpper() == "EUROPEAN")
                    {
                        EU euro = new EU();
                        euro.call = iscall;
                        euro.control = control;
                        euro.anti = anti;
                        euro.multithread = multithread;
                        double[] result = euro.getprice_sd(S0, SK, r, vol, T, steps, trials);
                        price = result[0];
                        double p1 = euro.getprice_sd(1.001 * S0, SK, r, vol, T, steps, trials)[0];
                        //        BeginInvoke(new Action(() => { progressBar1.Value = 27; }));
                        double p2 = euro.getprice_sd(0.999 * S0, SK, r, vol, T, steps, trials)[0];
                        //        BeginInvoke(new Action(() => { progressBar1.Value = 38; }));
                        delta = (p1 - p2) / (2 * 0.001 * S0);
                        gamma = (p1 + p2 - 2 * result[0]) / Math.Pow(0.001 * S0, 2); ;

                        double p3 = euro.getprice_sd(S0, SK, r, 1.001 * vol, T, steps, trials)[0];
                        //     BeginInvoke(new Action(() => { progressBar1.Value = 49; }));
                        double p4 = euro.getprice_sd(S0, SK, r, 0.999 * vol, T, steps, trials)[0];
                        //    BeginInvoke(new Action(() => { progressBar1.Value = 60; }));
                        vega = (p3 - p4) / (2 * 0.001 * vol);

                        double p5 = euro.getprice_sd(S0, SK, r, vol, 1.001 * T, steps, trials)[0];
                        //     BeginInvoke(new Action(() => { progressBar1.Value = 71; }));
                        theta = -(p5 - result[0]) / (0.001 * T);

                        double p6 = euro.getprice_sd(S0, SK, 1.001 * r, vol, T, steps, trials)[0];
                        //       BeginInvoke(new Action(() => { progressBar1.Value = 82; }));
                        double p7 = euro.getprice_sd(S0, SK, 0.999 * r, vol, T, steps, trials)[0];
                        //     BeginInvoke(new Action(() => { progressBar1.Value = 93; }));
                        rho = (p6 - p7) / (2 * 0.001 * r);
                    }

                    //Asian Option Implementation.
                    if (OptionType.ToUpper() == "ASIAN")
                    {
                        Asian asian = new Asian();
                        asian.call = iscall;
                        asian.control = control;
                        asian.anti = anti;
                        asian.multithread = multithread;
                        double[] result = asian.getprice_sd(S0, SK, r, vol, T, steps, trials);
                        price = result[0];
                        //    BeginInvoke(new Action(() => { progressBar1.Value = 16; }));
                        // Display the option price and standard error with the given calculation method.
                        //      BeginInvoke(new Action(() => { textBox8.Text = Convert.ToString(Math.Round(result[0], 5)); }));
                        //     BeginInvoke(new Action(() => { textBox14.Text = Convert.ToString(Math.Round(result[1], 5)); }));

                        double p1 = asian.getprice_sd(1.001 * S0, SK, r, vol, T, steps, trials)[0];
                        //   BeginInvoke(new Action(() => { progressBar1.Value = 27; }));
                        double p2 = asian.getprice_sd(0.999 * S0, SK, r, vol, T, steps, trials)[0];
                        //  BeginInvoke(new Action(() => { progressBar1.Value = 38; }));
                        delta = (p1 - p2) / (2 * 0.001 * S0);
                        gamma = (p1 + p2 - 2 * result[0]) / Math.Pow(0.001 * S0, 2); ;

                        double p3 = asian.getprice_sd(S0, SK, r, 1.001 * vol, T, steps, trials)[0];
                        // BeginInvoke(new Action(() => { progressBar1.Value = 49; }));
                        double p4 = asian.getprice_sd(S0, SK, r, 0.999 * vol, T, steps, trials)[0];
                        //        BeginInvoke(new Action(() => { progressBar1.Value = 60; }));
                        vega = (p3 - p4) / (2 * 0.001 * vol);

                        double p5 = asian.getprice_sd(S0, SK, r, vol, 1.001 * T, steps, trials)[0];
                        //      BeginInvoke(new Action(() => { progressBar1.Value = 71; }));
                        theta = -(p5 - result[0]) / (0.001 * T);

                        double p6 = asian.getprice_sd(S0, SK, 1.001 * r, vol, T, steps, trials)[0];
                        //       BeginInvoke(new Action(() => { progressBar1.Value = 82; }));
                        double p7 = asian.getprice_sd(S0, SK, 0.999 * r, vol, T, steps, trials)[0];
                        //       BeginInvoke(new Action(() => { progressBar1.Value = 93; }));
                        rho = (p6 - p7) / (2 * 0.001 * r);
                    }

                    //Barrier Option Implementation
                    if (OptionType.ToUpper() == "BARRIER")
                    {
                        Barrier barrier = new Barrier();
                        barrier.call = iscall;
                        barrier.control = control;
                        barrier.anti = anti;
                        barrier.multithread = multithread;

                        if (BarrierType == 1)
                        {
                            barrier.type = 1;
                        }
                        if (BarrierType == 2)
                        {
                            barrier.type = 2;
                        }
                        if (BarrierType == 3)
                        {
                            barrier.type = 3;
                        }
                        if (BarrierType == 4)
                        {
                            barrier.type = 4;
                        }
                        barrier.barrier_price = barrier_price;
                        // Handle the barrier price at the reasonable value.
                        if ((barrier.type <= 2) && (barrier.barrier_price < S0))
                        {
                            MessageBox.Show("Invalid Input!!! Barrier can not be lower than spot price for UP barrier option!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return null;
                        }
                        if ((barrier.type > 2) && (barrier.barrier_price > S0))
                        {
                            MessageBox.Show("Invalid Input!!! Barrier can not be higher than spot price for DOWN barrier option!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return null;
                        }
                        double[] result = barrier.getprice_sd(S0, SK, r, vol, T, steps, trials);
                        price = result[0];
                        double p1 = barrier.getprice_sd(1.001 * S0, SK, r, vol, T, steps, trials)[0];
                        //   BeginInvoke(new Action(() => { progressBar1.Value = 27; }));
                        double p2 = barrier.getprice_sd(0.999 * S0, SK, r, vol, T, steps, trials)[0];
                        // BeginInvoke(new Action(() => { progressBar1.Value = 38; }));
                        delta = (p1 - p2) / (2 * 0.001 * S0);
                        gamma = (p1 + p2 - 2 * result[0]) / Math.Pow(0.001 * S0, 2); ;

                        double p3 = barrier.getprice_sd(S0, SK, r, 1.001 * vol, T, steps, trials)[0];
                        // BeginInvoke(new Action(() => { progressBar1.Value = 49; }));
                        double p4 = barrier.getprice_sd(S0, SK, r, 0.999 * vol, T, steps, trials)[0];
                        //               BeginInvoke(new Action(() => { progressBar1.Value = 60; }));
                        vega = (p3 - p4) / (2 * 0.001 * vol);

                        double p5 = barrier.getprice_sd(S0, SK, r, vol, 1.001 * T, steps, trials)[0];
                        //             BeginInvoke(new Action(() => { progressBar1.Value = 71; }));
                        theta = -(p5 - result[0]) / (0.001 * T);

                        double p6 = barrier.getprice_sd(S0, SK, 1.001 * r, vol, T, steps, trials)[0];
                        double p7 = barrier.getprice_sd(S0, SK, 0.999 * r, vol, T, steps, trials)[0];
                        rho = (p6 - p7) / (2 * 0.001 * r);
                    }

                    //Lookback option implementation.
                    if (OptionType.ToUpper() == "LOOKBACK")
                    {
                        Lookback lookback = new Lookback();
                        lookback.call = iscall;
                        lookback.control = control;
                        lookback.anti = anti;
                        lookback.multithread = multithread;
                        double[] result = lookback.getprice_sd(S0, SK, r, vol, T, steps, trials);
                        price = result[0];
                        // Display the option price and standard error with the given calculation method.
                        double p1 = lookback.getprice_sd(1.001 * S0, SK, r, vol, T, steps, trials)[0];
                        double p2 = lookback.getprice_sd(0.999 * S0, SK, r, vol, T, steps, trials)[0];
                        delta = (p1 - p2) / (2 * 0.001 * S0);
                        gamma = (p1 + p2 - 2 * result[0]) / Math.Pow(0.001 * S0, 2); ;

                        double p3 = lookback.getprice_sd(S0, SK, r, 1.001 * vol, T, steps, trials)[0];
                        double p4 = lookback.getprice_sd(S0, SK, r, 0.999 * vol, T, steps, trials)[0];
                        vega = (p3 - p4) / (2 * 0.001 * vol);

                        double p5 = lookback.getprice_sd(S0, SK, r, vol, 1.001 * T, steps, trials)[0];
                        theta = -(p5 - result[0]) / (0.001 * T);

                        double p6 = lookback.getprice_sd(S0, SK, 1.001 * r, vol, T, steps, trials)[0];
                        double p7 = lookback.getprice_sd(S0, SK, 0.999 * r, vol, T, steps, trials)[0];
                        rho = (p6 - p7) / (2 * 0.001 * r);
                    }

                    //Digital option implementation.
                    if (OptionType.ToUpper() == "DIGITAL")
                    {
                        Digital digital = new Digital();
                        digital.call = iscall;
                        digital.control = control;
                        digital.anti = anti;
                        digital.multithread = multithread;
                        digital.rebate = rebate;
                        // Handle the rebate value at the reasonable value;
                        if (digital.rebate <= 0)
                        {
                            MessageBox.Show("Invalid Input!!! The rebate of digital option must be postive!!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return null;
                        }
                        double[] result = digital.getprice_sd(S0, SK, r, vol, T, steps, trials);
                        price = result[0];
                        double p1 = digital.getprice_sd(1.001 * S0, SK, r, vol, T, steps, trials)[0];
                        double p2 = digital.getprice_sd(0.999 * S0, SK, r, vol, T, steps, trials)[0];
                        delta = (p1 - p2) / (2 * 0.001 * S0);
                        gamma = (p1 + p2 - 2 * result[0]) / Math.Pow(0.001 * S0, 2); ;

                        double p3 = digital.getprice_sd(S0, SK, r, 1.001 * vol, T, steps, trials)[0];
                        double p4 = digital.getprice_sd(S0, SK, r, 0.999 * vol, T, steps, trials)[0];
                        vega = (p3 - p4) / (2 * 0.001 * vol);

                        double p5 = digital.getprice_sd(S0, SK, r, vol, 1.001 * T, steps, trials)[0];
                        theta = -(p5 - result[0]) / (0.001 * T);

                        double p6 = digital.getprice_sd(S0, SK, 1.001 * r, vol, T, steps, trials)[0];
                        double p7 = digital.getprice_sd(S0, SK, 0.999 * r, vol, T, steps, trials)[0];
                        rho = (p6 - p7) / (2 * 0.001 * r);

                    }

                    //Range option implementation.
                    if (OptionType.ToUpper() == "RANGE")
                    {
                        Range range = new Range();
                        range.control = control;
                        range.anti = anti;
                        range.multithread = multithread;
                        double[] result = range.getprice_sd(S0, SK, r, vol, T, steps, trials);
                        price = result[0];
                        double p1 = range.getprice_sd(1.001 * S0, SK, r, vol, T, steps, trials)[0];
                        double p2 = range.getprice_sd(0.999 * S0, SK, r, vol, T, steps, trials)[0];
                        delta = (p1 - p2) / (2 * 0.001 * S0);
                        gamma = (p1 + p2 - 2 * result[0]) / Math.Pow(0.001 * S0, 2); ;

                        double p3 = range.getprice_sd(S0, SK, r, 1.001 * vol, T, steps, trials)[0];
                        double p4 = range.getprice_sd(S0, SK, r, 0.999 * vol, T, steps, trials)[0];
                        vega = (p3 - p4) / (2 * 0.001 * vol);

                        double p5 = range.getprice_sd(S0, SK, r, vol, 1.001 * T, steps, trials)[0];
                        theta = -(p5 - result[0]) / (0.001 * T);

                        double p6 = range.getprice_sd(S0, SK, 1.001 * r, vol, T, steps, trials)[0];
                        double p7 = range.getprice_sd(S0, SK, 0.999 * r, vol, T, steps, trials)[0];
                        rho = (p6 - p7) / (2 * 0.001 * r);

                    }

                    return new double[] { price, delta, gamma, vega, theta, rho };
                }
                else
                {
                    MessageBox.Show("Invalid Input!!! Make sure all of them no less than 0 and steps should be at least 2!!!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return null;
                }
            }
            catch
            {
                MessageBox.Show("The number of trials and steps should be an integer!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        // Notification for users to pass into valid intputs.
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox1.Text, out num))
            {
               MessageBox.Show("Please enter a number!");
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox2.Text, out num))
            {
                MessageBox.Show("Please enter a number!");
            }
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox3.Text, out num))
            {
                MessageBox.Show("Please enter a number!");
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox4.Text, out num))
            {
                MessageBox.Show( "Please enter a number!");
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox5.Text, out num))
            {
                MessageBox.Show( "Please enter a number!");
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            int num;
            if (!int.TryParse(textBox6.Text, out num))
            {
                MessageBox.Show("Please enter an integer number and it should be larger than 2!");
            }
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            int num;
            if (!int.TryParse(textBox7.Text, out num))
            {
                MessageBox.Show("Please enter an integer number!");
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
            {
                groupBox4.Enabled = true;
            }
            else
            {
                groupBox4.Enabled = false;
            }
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton8.Checked)
            {
                groupBox1.Enabled = false;
            }
            else
            {
                groupBox1.Enabled = true;
            }
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton7.Checked)
            {
                groupBox5.Enabled = true;
            }
            else
            {
                groupBox5.Enabled = false;
            }
        }

        private void textBox16_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox7.Text, out num))
            {
                MessageBox.Show("Please enter a number!");
            }
        }

        private void textBox17_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox7.Text, out num))
            {
                MessageBox.Show( "Please enter a number!");
            }
        }
    }
}

