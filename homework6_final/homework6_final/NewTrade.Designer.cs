﻿namespace assignmentfinal
{
    partial class NewTrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.instrumentSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.homework6_finalDataSet4 = new homework6_final.homework6_finalDataSet4();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.instrumentSetTableAdapter = new homework6_final.homework6_finalDataSet4TableAdapters.InstrumentSetTableAdapter();
            this.homework6_finalDataSet9 = new homework6_final.homework6_finalDataSet9();
            this.instrumentSetBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.instrumentSetTableAdapter1 = new homework6_final.homework6_finalDataSet9TableAdapters.InstrumentSetTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentSetBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(68, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Direction:";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton1.Location = new System.Drawing.Point(214, 46);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(53, 21);
            this.radioButton1.TabIndex = 14;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Buy";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(275, 46);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(52, 21);
            this.radioButton2.TabIndex = 15;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Sell";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 88);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "Quantity:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(214, 88);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(152, 22);
            this.textBox1.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(68, 133);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "Instrument:";
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.instrumentSetBindingSource1;
            this.comboBox1.DisplayMember = "Ticker";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(214, 133);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(160, 24);
            this.comboBox1.TabIndex = 19;
            // 
            // instrumentSetBindingSource
            // 
            this.instrumentSetBindingSource.DataMember = "InstrumentSet";
            this.instrumentSetBindingSource.DataSource = this.homework6_finalDataSet4;
            // 
            // homework6_finalDataSet4
            // 
            this.homework6_finalDataSet4.DataSetName = "homework6_finalDataSet4";
            this.homework6_finalDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(68, 180);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 20;
            this.label4.Text = "Price:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(214, 180);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(152, 22);
            this.textBox2.TabIndex = 21;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(71, 228);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 31);
            this.button1.TabIndex = 22;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(214, 228);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 31);
            this.button2.TabIndex = 23;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // instrumentSetTableAdapter
            // 
            this.instrumentSetTableAdapter.ClearBeforeFill = true;
            // 
            // homework6_finalDataSet9
            // 
            this.homework6_finalDataSet9.DataSetName = "homework6_finalDataSet9";
            this.homework6_finalDataSet9.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // instrumentSetBindingSource1
            // 
            this.instrumentSetBindingSource1.DataMember = "InstrumentSet";
            this.instrumentSetBindingSource1.DataSource = this.homework6_finalDataSet9;
            // 
            // instrumentSetTableAdapter1
            // 
            this.instrumentSetTableAdapter1.ClearBeforeFill = true;
            // 
            // NewTrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 305);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label1);
            this.Name = "NewTrade";
            this.Text = "NewTrade";
            this.Load += new System.EventHandler(this.NewTrade_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.instrumentSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentSetBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private homework6_final.homework6_finalDataSet4 homework6_finalDataSet4;
        private System.Windows.Forms.BindingSource instrumentSetBindingSource;
        private homework6_final.homework6_finalDataSet4TableAdapters.InstrumentSetTableAdapter instrumentSetTableAdapter;
        private homework6_final.homework6_finalDataSet9 homework6_finalDataSet9;
        private System.Windows.Forms.BindingSource instrumentSetBindingSource1;
        private homework6_final.homework6_finalDataSet9TableAdapters.InstrumentSetTableAdapter instrumentSetTableAdapter1;
    }
}