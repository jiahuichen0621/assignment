﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace homework6_final
{
    public partial class NewInstType : Form
    {
        ChenentityContainer MC = new ChenentityContainer();
        public NewInstType()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Add new instrument type into the database.
            MC.InstTypeSet.Add(new InstType()
            {
                TypeName = Convert.ToString(textBox1.Text)
            });
            MC.SaveChanges();
            MessageBox.Show("Add new instrument type successfully!");
            this.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
