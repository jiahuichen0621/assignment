﻿namespace assignmentfinal
{
    partial class NewInterestRate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.homework6_finalDataSet3 = new homework6_final.homework6_finalDataSet3();
            this.interestRateSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.interestRateSetTableAdapter = new homework6_final.homework6_finalDataSet3TableAdapters.InterestRateSetTableAdapter();
            this.tenorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interestRateSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tenorDataGridViewTextBoxColumn,
            this.rateDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.interestRateSetBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(79, 60);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(327, 269);
            this.dataGridView1.TabIndex = 5;
            // 
            // homework6_finalDataSet3
            // 
            this.homework6_finalDataSet3.DataSetName = "homework6_finalDataSet3";
            this.homework6_finalDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // interestRateSetBindingSource
            // 
            this.interestRateSetBindingSource.DataMember = "InterestRateSet";
            this.interestRateSetBindingSource.DataSource = this.homework6_finalDataSet3;
            // 
            // interestRateSetTableAdapter
            // 
            this.interestRateSetTableAdapter.ClearBeforeFill = true;
            // 
            // tenorDataGridViewTextBoxColumn
            // 
            this.tenorDataGridViewTextBoxColumn.DataPropertyName = "Tenor";
            this.tenorDataGridViewTextBoxColumn.HeaderText = "Tenor";
            this.tenorDataGridViewTextBoxColumn.Name = "tenorDataGridViewTextBoxColumn";
            // 
            // rateDataGridViewTextBoxColumn
            // 
            this.rateDataGridViewTextBoxColumn.DataPropertyName = "Rate";
            this.rateDataGridViewTextBoxColumn.HeaderText = "Rate";
            this.rateDataGridViewTextBoxColumn.Name = "rateDataGridViewTextBoxColumn";
            // 
            // NewInterestRate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 389);
            this.Controls.Add(this.dataGridView1);
            this.Name = "NewInterestRate";
            this.Text = "NewInterestRate";
            this.Load += new System.EventHandler(this.NewInterestRate_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interestRateSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private homework6_final.homework6_finalDataSet3 homework6_finalDataSet3;
        private System.Windows.Forms.BindingSource interestRateSetBindingSource;
        private homework6_final.homework6_finalDataSet3TableAdapters.InterestRateSetTableAdapter interestRateSetTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rateDataGridViewTextBoxColumn;
    }
}