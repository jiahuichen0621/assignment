﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using homework6_final;


namespace assignmentfinal
{
    public partial class NewHistPrice : Form
    {
        ChenentityContainer MC = new ChenentityContainer();
        public NewHistPrice()
        {
            InitializeComponent();
            // for the instrument field, you can only choose stocks.
            var t = from p in MC.InstrumentSet
                    where p.InstType.TypeName.ToUpper() == "STOCK"
                    select p;
            foreach (Instrument kik in t)
            {
                comboBox1.Items.Add(kik.Ticker);
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void NewHistPrice_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“homework6_finalDataSet6.InstrumentSet”中。您可以根据需要移动或删除它。
            this.instrumentSetTableAdapter.Fill(this.homework6_finalDataSet6.InstrumentSet);

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            // Add the user-added data into the database, one at a time. So this means you can only add one entry and then click save to save the changes.

            Instrument t = (from p in MC.InstrumentSet
                            where p.Ticker == comboBox1.Text
                            select p).First();
            MC.PriceSet.Add(new Price()
            {
                Date = dateTimePicker1.Value,
                ClosingPrice = Convert.ToDouble(textBox1.Text),
                Instrument = t,
                InstrumentId = t.Id

            });
            MC.SaveChanges();
            MessageBox.Show("Saving the added historical prices to database successfully!");

            this.Dispose();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            
            this.Dispose();
        }
    }
}
