﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace homework6_final
{
    public partial class HistoricalPrice : Form
    {
        public HistoricalPrice()
        {
            InitializeComponent();
        }

        private void HistoricalPrice_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“homework6_finalDataSet10.PriceSet”中。您可以根据需要移动或删除它。
            this.priceSetTableAdapter1.Fill(this.homework6_finalDataSet10.PriceSet);
            // TODO: 这行代码将数据加载到表“homework6_finalDataSet8.InstrumentSet”中。您可以根据需要移动或删除它。
            this.instrumentSetTableAdapter.Fill(this.homework6_finalDataSet8.InstrumentSet);
            // TODO: 这行代码将数据加载到表“homework6_finalDataSet7.PriceSet”中。您可以根据需要移动或删除它。
            this.priceSetTableAdapter.Fill(this.homework6_finalDataSet7.PriceSet);

        }
        
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Calculate the implied volatility based on selected prices.
            double g = 0;
            double t = 0;
            foreach (DataGridViewCell c in dataGridView1.SelectedCells)
            {
                if (c.Value != null && double.TryParse(c.Value.ToString(), out t))
                {
                    g += t;
                }
            }
            double sum = 0;
            foreach (DataGridViewCell c in dataGridView1.SelectedCells)
            {
                if (c.Value != null && double.TryParse(c.Value.ToString(), out t))
                {
                    sum += Math.Pow((t - g), 2);
                }
            }
            double s = Math.Sqrt(sum / ((double)dataGridView1.SelectedCells.Count - 1));
            s = s * Math.Sqrt(252);
            textBox1.Text = s.ToString() + "%";
        }

        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
           
        }
    }
}
