﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using homework6_final;

namespace assignmentfinal
{
    public partial class NewInstrument : Form
    {
        ChenentityContainer MC = new ChenentityContainer();
        public NewInstrument()
        {
            InitializeComponent();
            //for the underlying field, you can only choose stocks as underlyding.
            var t = from p in MC.InstrumentSet
                    where p.InstType.TypeName.ToUpper() == "STOCK"
                    select p;
            foreach (Instrument kik in t)
            {
                comboBox2.Items.Add(kik.Ticker);
            }
           

        }
        private void NewInstrument_Load_1(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“homework6_finalDataSet2.InstrumentSet”中。您可以根据需要移动或删除它。
            this.instrumentSetTableAdapter.Fill(this.homework6_finalDataSet2.InstrumentSet);
            // TODO: 这行代码将数据加载到表“homework6_finalDataSet1.InstTypeSet”中。您可以根据需要移动或删除它。
            this.instTypeSetTableAdapter.Fill(this.homework6_finalDataSet1.InstTypeSet);
          

        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            // For different selection of instrument type, set different accessibility of those comboBox and textBox accordingly.
            if (comboBox1.Text.ToUpper() == "STOCK")
            {
                comboBox2.Enabled = false;
                textBox4.Enabled = false;
                textBox5.Enabled = false;
            }
            else
            {
                comboBox2.Enabled = true;
                textBox4.Enabled = true;
                textBox5.Enabled = true;
            }
            if (comboBox1.Text.ToUpper() == "RANGE")
            {
                radioButton1.Enabled = false;
                radioButton2.Enabled = false;
                radioButton3.Enabled = false;
            }
            else
            {
                radioButton1.Enabled = true;
                radioButton2.Enabled = true;
                radioButton3.Enabled = true;
            }
            if (comboBox1.Text.ToUpper() == "DIGITAL")
            {
                groupBox5.Enabled = true;
            }
            else
            {
                groupBox5.Enabled = false;
            }
            if (comboBox1.Text.ToUpper() == "BARRIER")
            {
                groupBox4.Enabled = true;
            }
            else
            {
                groupBox4.Enabled = false;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            // Allow you to add new instruments of a certain inst type.

            InstType t = (from p in MC.InstTypeSet
                          where p.TypeName == comboBox1.Text
                          select p).First();



            MC.InstrumentSet.Add(new Instrument()
            {
                CompanyName = textBox1.Text == string.Empty ? null : Convert.ToString(textBox1.Text),
                Ticker = textBox2.Text == string.Empty ? null : Convert.ToString(textBox2.Text),
                Exchange = textBox3.Text == string.Empty ? null : Convert.ToString(textBox3.Text),
                Underlying = comboBox2.Text == string.Empty ? null : t.TypeName.ToUpper() == "STOCK" ? null : Convert.ToString(comboBox2.Text),
                Strike = t.TypeName.ToUpper() == "STOCK" ? 0 : textBox4.Text == string.Empty ? 0 : Convert.ToDouble(textBox4.Text),
                Tenor = t.TypeName.ToUpper() == "STOCK" ? 0 : textBox5.Text == string.Empty ? 0 : Convert.ToDouble(textBox5.Text),
                IsCall = radioButton2.Checked,
                InstTypeId = t.Id,
                InstType = t,
                Barrier = textBox16.Text == string.Empty ? 0 : t.TypeName.ToUpper() == "BARRIER" ? Convert.ToDouble(textBox16.Text) : 0,
                BarrierType = t.TypeName.ToUpper() == "BARRIER" ? radioButton9.Checked ? "Up and In" : radioButton10.Checked ? "Up and Out" : radioButton11.Checked ? "Down and In" : radioButton12.Checked ? "Down and Out" : null : null,
                Rebate = textBox17.Text == string.Empty ? 0 : t.TypeName.ToUpper() == "DIGITAL" ? Convert.ToDouble(textBox17.Text) : 0

            });
            MC.SaveChanges();
            MessageBox.Show("New instrument added successfully!");
            this.Dispose();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
