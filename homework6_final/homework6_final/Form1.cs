﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using assignmentfinal;
using MC_Simulator;

namespace homework6_final
{
    public partial class Form1 : Form
    {
        ChenentityContainer MC = new ChenentityContainer();
        public Form1()
        {
            InitializeComponent();
            textBox1.Text = "50";
        }

        private void instrumentTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Call the new form to add new instrument type.
            NewInstType newForm = new NewInstType();
            newForm.ShowDialog();
        }

        private void instrumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //call the new form to add new instrument.
            NewInstrument newForm1 = new NewInstrument();
            newForm1.ShowDialog();
        }

        private void tradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Call the new form to add new trades.
            NewTrade newForm = new NewTrade();
            newForm.ShowDialog();
        }

        private void interestRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Curate Interest rate.
            InsertInterestRate form1 = new InsertInterestRate();
            form1.ShowDialog();
        }

        private void historicalPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Curate historical price for instrument.

            NewHistPrice newForm2 = new NewHistPrice();
            newForm2.ShowDialog();
        }

        private void refreshTradesFromDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Call the RefreshTrades() method.
            RefreshTrades();
        }
        private void RefreshTrades()
        {
            listView2.Items.Clear();
            ListViewItem i;
            foreach (Trade t in MC.TradeSet)
            {
                i = new ListViewItem();
                i.Text = t.Id.ToString();
                i.SubItems.Add(t.IsBuy ? "BUY" : "SELL");
                i.SubItems.Add(t.Quantity.ToString());
                i.SubItems.Add(t.Instrument.Ticker);
                i.SubItems.Add(t.Instrument.InstType.TypeName);
                i.SubItems.Add(t.Price.ToString());
                i.SubItems.Add("0");
                i.SubItems.Add("0");
                i.SubItems.Add("0");
                i.SubItems.Add("0");
                i.SubItems.Add("0");
                i.SubItems.Add("0");
                i.SubItems.Add("0");
                listView2.Items.Add(i);
            }
        }

        private void priceBookUsingSimulationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MC_Simulator.Form1 a = new MC_Simulator.Form1();
            double[] result;
            listView2.Items.Clear();
            ListViewItem i;
            // For each trade do the following things.
            foreach (Trade t in MC.TradeSet)
            {
                i = new ListViewItem();
                i.Text = t.Id.ToString();
                i.SubItems.Add(t.IsBuy ? "BUY" : "SELL");
                i.SubItems.Add(t.Quantity.ToString());
                i.SubItems.Add(t.Instrument.Ticker);
                i.SubItems.Add(t.Instrument.InstType.TypeName);
                i.SubItems.Add(t.Price.ToString());

                // Check the Instrument type and do different things according to the inst type.
                if (t.Instrument.InstType.TypeName.ToUpper() == "STOCK")
                {
                    var qq = (from p in MC.InstrumentSet
                              where p.Ticker == t.Instrument.Ticker
                              select p).First();
                    // Find out the Underlying price.
                    double S0 = qq.Price.Last().ClosingPrice;
                    // Assign the values to other fields such as Market Price, P&L and Greeks and etc.
                    i.SubItems.Add(S0.ToString());
                    i.SubItems.Add(t.IsBuy ? ((S0 - t.Price) * t.Quantity).ToString() : ((t.Price - S0) * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (1.0 * t.Quantity).ToString() : (-1.0 * t.Quantity).ToString());
                    i.SubItems.Add("0");
                    i.SubItems.Add("0");
                    i.SubItems.Add("0");
                    i.SubItems.Add("0");
                }
                else if (t.Instrument.InstType.TypeName.ToUpper() == "EUROPEAN")
                {

                    var qq = (from p in MC.InstrumentSet
                              where p.Ticker == t.Instrument.Underlying
                              select p).First();
                    double S0 = qq.Price.Last().ClosingPrice;

                    // For options, use the MC_Simulator to get back the market price and all the greeks, then assign them accordingly.
                    result = a.Calc("EUROPEAN", t.Instrument.IsCall, S0, Convert.ToDouble(t.Instrument.Strike), 0.05, Convert.ToDouble(textBox1.Text) / 100, Convert.ToDouble(t.Instrument.Tenor), 0, 0, 0);
                    i.SubItems.Add(result[0].ToString());
                    i.SubItems.Add(t.IsBuy ? ((result[0] - t.Price) * t.Quantity).ToString() : ((t.Price - result[0]) * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[1] * t.Quantity).ToString() : (-1 * result[1] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[2] * t.Quantity).ToString() : (-1 * result[2] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[3] * t.Quantity).ToString() : (-1 * result[3] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[4] * t.Quantity).ToString() : (-1 * result[4] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[5] * t.Quantity).ToString() : (-1 * result[5] * t.Quantity).ToString());

                }
                else if (t.Instrument.InstType.TypeName.ToUpper() == "ASIAN")
                {
                    // Same as above, except that this is for asian option.
                    var qq = (from p in MC.InstrumentSet
                              where p.Ticker == t.Instrument.Underlying
                              select p).First();
                    double S0 = qq.Price.Last().ClosingPrice;
                    result = a.Calc("ASIAN", t.Instrument.IsCall, S0, Convert.ToDouble(t.Instrument.Strike), 0.05, Convert.ToDouble(textBox1.Text) / 100, Convert.ToDouble(t.Instrument.Tenor), 0, 0, 0);
                    i.SubItems.Add(result[0].ToString());
                    i.SubItems.Add(t.IsBuy ? ((result[0] - t.Price) * t.Quantity).ToString() : ((t.Price - result[0]) * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[1] * t.Quantity).ToString() : (-1 * result[1] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[2] * t.Quantity).ToString() : (-1 * result[2] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[3] * t.Quantity).ToString() : (-1 * result[3] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[4] * t.Quantity).ToString() : (-1 * result[4] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[5] * t.Quantity).ToString() : (-1 * result[5] * t.Quantity).ToString());
                }
                else if (t.Instrument.InstType.TypeName.ToUpper() == "BARRIER")
                {
                    // This is for barrier option, others the same.
                    var qq = (from p in MC.InstrumentSet
                              where p.Ticker == t.Instrument.Underlying
                              select p).First();
                    double S0 = qq.Price.Last().ClosingPrice;
                    int barrier_type = t.Instrument.BarrierType.ToUpper() == "UP AND IN" ? 1 : t.Instrument.BarrierType.ToUpper() == "UP AND OUT" ? 2 : t.Instrument.BarrierType.ToUpper() == "DOWN AND IN" ? 3 : t.Instrument.BarrierType.ToUpper() == "DOWN AND OUT" ? 4 : 0;
                    result = a.Calc("BARRIER", t.Instrument.IsCall, S0, Convert.ToDouble(t.Instrument.Strike), 0.05, Convert.ToDouble(textBox1.Text) / 100, Convert.ToDouble(t.Instrument.Tenor), barrier_type, Convert.ToDouble(t.Instrument.Barrier), 0);
                    i.SubItems.Add(result[0].ToString());
                    i.SubItems.Add(t.IsBuy ? ((result[0] - t.Price) * t.Quantity).ToString() : ((t.Price - result[0]) * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[1] * t.Quantity).ToString() : (-1 * result[1] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[2] * t.Quantity).ToString() : (-1 * result[2] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[3] * t.Quantity).ToString() : (-1 * result[3] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[4] * t.Quantity).ToString() : (-1 * result[4] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[5] * t.Quantity).ToString() : (-1 * result[5] * t.Quantity).ToString());
                }
                else if (t.Instrument.InstType.TypeName.ToUpper() == "LOOKBACK")
                {
                    // Lookback option.
                    var qq = (from p in MC.InstrumentSet
                              where p.Ticker == t.Instrument.Underlying
                              select p).First();
                    double S0 = qq.Price.Last().ClosingPrice;
                    result = a.Calc("LOOKBACK", t.Instrument.IsCall, S0, Convert.ToDouble(t.Instrument.Strike), 0.05, Convert.ToDouble(textBox1.Text) / 100, Convert.ToDouble(t.Instrument.Tenor), 0, 0, 0);
                    i.SubItems.Add(result[0].ToString());
                    i.SubItems.Add(t.IsBuy ? ((result[0] - t.Price) * t.Quantity).ToString() : ((t.Price - result[0]) * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[1] * t.Quantity).ToString() : (-1 * result[1] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[2] * t.Quantity).ToString() : (-1 * result[2] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[3] * t.Quantity).ToString() : (-1 * result[3] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[4] * t.Quantity).ToString() : (-1 * result[4] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[5] * t.Quantity).ToString() : (-1 * result[5] * t.Quantity).ToString());
                }
                else if (t.Instrument.InstType.TypeName.ToUpper() == "DIGITAL")
                {
                    // Digital option.
                    var qq = (from p in MC.InstrumentSet
                              where p.Ticker == t.Instrument.Underlying
                              select p).First();
                    double S0 = qq.Price.Last().ClosingPrice;
                    double rebate_1 = Convert.ToDouble(t.Instrument.Rebate);
                    result = a.Calc("DIGITAL", t.Instrument.IsCall, S0, Convert.ToDouble(t.Instrument.Strike), 0.05, Convert.ToDouble(textBox1.Text) / 100, Convert.ToDouble(t.Instrument.Tenor), 0, 0, rebate_1);
                    i.SubItems.Add(result[0].ToString());
                    i.SubItems.Add(t.IsBuy ? ((result[0] - t.Price) * t.Quantity).ToString() : ((t.Price - result[0]) * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[1] * t.Quantity).ToString() : (-1 * result[1] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[2] * t.Quantity).ToString() : (-1 * result[2] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[3] * t.Quantity).ToString() : (-1 * result[3] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[4] * t.Quantity).ToString() : (-1 * result[4] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[5] * t.Quantity).ToString() : (-1 * result[5] * t.Quantity).ToString());
                }
                else if (t.Instrument.InstType.TypeName.ToUpper() == "RANGE")
                {
                    // Range Option.
                    var qq = (from p in MC.InstrumentSet
                              where p.Ticker == t.Instrument.Underlying
                              select p).First();
                    double S0 = qq.Price.Last().ClosingPrice;

                    result = a.Calc("RANGE", t.Instrument.IsCall, S0, Convert.ToDouble(t.Instrument.Strike), 0.05, Convert.ToDouble(textBox1.Text) / 100, Convert.ToDouble(t.Instrument.Tenor), 0, 0, 0);
                    i.SubItems.Add(result[0].ToString());
                    i.SubItems.Add(t.IsBuy ? ((result[0] - t.Price) * t.Quantity).ToString() : ((t.Price - result[0]) * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[1] * t.Quantity).ToString() : (-1 * result[1] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (Math.Round(result[2], 5) * t.Quantity).ToString() : (-1 * Math.Round(result[2], 5) * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[3] * t.Quantity).ToString() : (-1 * result[3] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[4] * t.Quantity).ToString() : (-1 * result[4] * t.Quantity).ToString());
                    i.SubItems.Add(t.IsBuy ? (result[5] * t.Quantity).ToString() : (-1 * result[5] * t.Quantity).ToString());
                }
                listView2.Items.Add(i);
            }

            MessageBox.Show("Pricing Done!");
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // When you select different trades, the summary of the total P&L and greeks will be shown in the listView1.
            listView1.Items.Clear();
            ListViewItem i = new ListViewItem();
            int t = listView2.SelectedItems.Count;
            int[] looping = new int[t];
            for (int k = 0; k < t; k++)
            {
                looping[k] = listView2.SelectedIndices[k];
            }
            double a1 = 0, a2 = 0, a3 = 0, a4 = 0, a5 = 0, a6 = 0;
            foreach (int k in looping)
            {
                a1 = a1 + Convert.ToDouble(listView2.Items[k].SubItems[7].Text);
                a2 = a2 + Convert.ToDouble(listView2.Items[k].SubItems[8].Text);
                a3 = a3 + Convert.ToDouble(listView2.Items[k].SubItems[9].Text);
                a4 = a4 + Convert.ToDouble(listView2.Items[k].SubItems[10].Text);
                a5 = a5 + Convert.ToDouble(listView2.Items[k].SubItems[11].Text);
                a6 = a6 + Convert.ToDouble(listView2.Items[k].SubItems[12].Text);
            }
            i.Text = a1.ToString();
            i.SubItems.Add(a2.ToString());
            i.SubItems.Add(a3.ToString());
            i.SubItems.Add(a4.ToString());
            i.SubItems.Add(a5.ToString());
            i.SubItems.Add(a6.ToString());

            listView1.Items.Add(i);
        }

        private void historicalPricesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistoricalPrice newForm2 = new HistoricalPrice();
            newForm2.ShowDialog();
        }

        private void interestRateToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            NewInterestRate form1 = new NewInterestRate();
            form1.ShowDialog();
        }
        

        private void deleteToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            // Allows you to delete trades from listView2 using right click, one at a time.
            int t = listView2.SelectedIndices[0];

            listView2.Items[t].Remove();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Allows you to delete trades from listView2 using right click, one at a time.
            int t = listView2.SelectedIndices[0];

            listView2.Items[t].Remove();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
