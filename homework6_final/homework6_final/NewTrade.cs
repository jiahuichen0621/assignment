﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using homework6_final;

namespace assignmentfinal
{
    public partial class NewTrade : Form
    {
        ChenentityContainer MC = new ChenentityContainer();
        public NewTrade()
        {
            InitializeComponent();
        }
        private void NewTrade_Load_1(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“homework6_finalDataSet9.InstrumentSet”中。您可以根据需要移动或删除它。
            this.instrumentSetTableAdapter1.Fill(this.homework6_finalDataSet9.InstrumentSet);
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Instrument t = (from p in MC.InstrumentSet
                            where p.Ticker == comboBox1.Text
                            select p).First();

            // Add new trades into the database.

            MC.TradeSet.Add(new Trade()
            {
                Instrument = t,
                IsBuy = (radioButton1.Checked ? true : false),
                Price = Convert.ToDouble(textBox2.Text),
                Quantity = Convert.ToInt32(textBox1.Text),
                Timestamp = DateTime.Now
                
            });
            // Also if you are adding a trade of stocks, store the stock price into the Price table automatically.
            if (t.InstType.TypeName.ToUpper() == "STOCK")
            {
                MC.PriceSet.Add(new Price()
                {
                    ClosingPrice = Convert.ToDouble(textBox2.Text),
                    Date = DateTime.Now,
                    Instrument = t,
                    InstrumentId = t.Id


                });
            }
            MC.SaveChanges();
            MessageBox.Show("New trade added successfully!");
            this.Dispose();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
