﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using homework6_final;

namespace assignmentfinal
{
    public partial class InsertInterestRate : Form
    {
        ChenentityContainer MC = new ChenentityContainer();
        public InsertInterestRate()
        {
            InitializeComponent();

        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            //Add the new interest rate into the database.
            MC.InterestRateSet.Add(new InterestRate()
            {
                Tenor = Convert.ToDouble(textBox1.Text),
                Rate = Convert.ToDouble(textBox2.Text)
            });
            MC.SaveChanges();
            MessageBox.Show("Saving the added interest rate to database successfully!");

            this.Dispose();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            
            this.Dispose();
        }
    }
}
