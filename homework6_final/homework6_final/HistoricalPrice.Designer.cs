﻿namespace homework6_final
{
    partial class HistoricalPrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.closingPriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceSetBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.homework6_finalDataSet10 = new homework6_final.homework6_finalDataSet10();
            this.priceSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.homework6_finalDataSet7 = new homework6_final.homework6_finalDataSet7();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.instrumentSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.homework6_finalDataSet8 = new homework6_final.homework6_finalDataSet8();
            this.label1 = new System.Windows.Forms.Label();
            this.priceSetTableAdapter = new homework6_final.homework6_finalDataSet7TableAdapters.PriceSetTableAdapter();
            this.instrumentSetTableAdapter = new homework6_final.homework6_finalDataSet8TableAdapters.InstrumentSetTableAdapter();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.priceSetTableAdapter1 = new homework6_final.homework6_finalDataSet10TableAdapters.PriceSetTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceSetBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet8)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(374, 341);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(132, 22);
            this.textBox1.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(240, 346);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Implied Volatility:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateDataGridViewTextBoxColumn,
            this.closingPriceDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.priceSetBindingSource1;
            this.dataGridView1.Location = new System.Drawing.Point(232, 119);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(317, 201);
            this.dataGridView1.TabIndex = 13;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            // 
            // closingPriceDataGridViewTextBoxColumn
            // 
            this.closingPriceDataGridViewTextBoxColumn.DataPropertyName = "ClosingPrice";
            this.closingPriceDataGridViewTextBoxColumn.HeaderText = "ClosingPrice";
            this.closingPriceDataGridViewTextBoxColumn.Name = "closingPriceDataGridViewTextBoxColumn";
            // 
            // priceSetBindingSource1
            // 
            this.priceSetBindingSource1.DataMember = "PriceSet";
            this.priceSetBindingSource1.DataSource = this.homework6_finalDataSet10;
            // 
            // homework6_finalDataSet10
            // 
            this.homework6_finalDataSet10.DataSetName = "homework6_finalDataSet10";
            this.homework6_finalDataSet10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // priceSetBindingSource
            // 
            this.priceSetBindingSource.DataMember = "PriceSet";
            this.priceSetBindingSource.DataSource = this.homework6_finalDataSet7;
            // 
            // homework6_finalDataSet7
            // 
            this.homework6_finalDataSet7.DataSetName = "homework6_finalDataSet7";
            this.homework6_finalDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.instrumentSetBindingSource;
            this.comboBox1.DisplayMember = "Ticker";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(346, 77);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(160, 24);
            this.comboBox1.TabIndex = 12;
            // 
            // instrumentSetBindingSource
            // 
            this.instrumentSetBindingSource.DataMember = "InstrumentSet";
            this.instrumentSetBindingSource.DataSource = this.homework6_finalDataSet8;
            // 
            // homework6_finalDataSet8
            // 
            this.homework6_finalDataSet8.DataSetName = "homework6_finalDataSet8";
            this.homework6_finalDataSet8.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(240, 77);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Instrument:";
            // 
            // priceSetTableAdapter
            // 
            this.priceSetTableAdapter.ClearBeforeFill = true;
            // 
            // instrumentSetTableAdapter
            // 
            this.instrumentSetTableAdapter.ClearBeforeFill = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(126, 28);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(125, 24);
            this.deleteToolStripMenuItem.Text = "delete";
            // 
            // priceSetTableAdapter1
            // 
            this.priceSetTableAdapter1.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(280, 387);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(202, 36);
            this.button1.TabIndex = 16;
            this.button1.Text = "Calculate implie Volatility";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem1});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(127, 28);
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(210, 24);
            this.deleteToolStripMenuItem1.Text = "Delete";
            this.deleteToolStripMenuItem1.Click += new System.EventHandler(this.deleteToolStripMenuItem1_Click);
            // 
            // HistoricalPrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Name = "HistoricalPrice";
            this.Text = "HistoricalPrice";
            this.Load += new System.EventHandler(this.HistoricalPrice_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceSetBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homework6_finalDataSet8)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private homework6_finalDataSet7 homework6_finalDataSet7;
        private System.Windows.Forms.BindingSource priceSetBindingSource;
        private homework6_finalDataSet7TableAdapters.PriceSetTableAdapter priceSetTableAdapter;
        private homework6_finalDataSet8 homework6_finalDataSet8;
        private System.Windows.Forms.BindingSource instrumentSetBindingSource;
        private homework6_finalDataSet8TableAdapters.InstrumentSetTableAdapter instrumentSetTableAdapter;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private homework6_finalDataSet10 homework6_finalDataSet10;
        private System.Windows.Forms.BindingSource priceSetBindingSource1;
        private homework6_finalDataSet10TableAdapters.PriceSetTableAdapter priceSetTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn closingPriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
    }
}