
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/27/2018 13:22:25
-- Generated from EDMX file: C:\Users\JiahuiChen\source\repos\homework6_final\homework6_final\Chenentity.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [homework6_final];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'InstTypeSet'
CREATE TABLE [dbo].[InstTypeSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TypeName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'InstrumentSet'
CREATE TABLE [dbo].[InstrumentSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CompanyName] nvarchar(max)  NULL,
    [Ticker] nvarchar(max)  NOT NULL,
    [Exchange] nvarchar(max)  NULL,
    [Underlying] nvarchar(max)  NULL,
    [Strike] float  NULL,
    [Tenor] float  NULL,
    [IsCall] bit  NOT NULL,
    [InstTypeId] int  NOT NULL,
    [Barrier] float  NULL,
    [BarrierType] nvarchar(max)  NULL,
    [Rebate] float  NULL,
    [InstType_Id] int  NOT NULL
);
GO

-- Creating table 'PriceSet'
CREATE TABLE [dbo].[PriceSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [ClosingPrice] float  NOT NULL,
    [InstrumentId] int  NOT NULL,
    [Instrument_Id] int  NOT NULL
);
GO

-- Creating table 'TradeSet'
CREATE TABLE [dbo].[TradeSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsBuy] bit  NOT NULL,
    [Quantity] int  NOT NULL,
    [InstrumentId] int  NOT NULL,
    [Price] float  NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [Instrument_Id] int  NOT NULL
);
GO

-- Creating table 'TotalSet'
CREATE TABLE [dbo].[TotalSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TotalPL] float  NOT NULL,
    [TotalDelta] float  NOT NULL,
    [TotalGamma] float  NOT NULL,
    [TotalTheta] float  NOT NULL,
    [TotalRho] float  NOT NULL,
    [TotalVega] float  NOT NULL
);
GO

-- Creating table 'InterestRateSet'
CREATE TABLE [dbo].[InterestRateSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Tenor] float  NOT NULL,
    [Rate] float  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'InstTypeSet'
ALTER TABLE [dbo].[InstTypeSet]
ADD CONSTRAINT [PK_InstTypeSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InstrumentSet'
ALTER TABLE [dbo].[InstrumentSet]
ADD CONSTRAINT [PK_InstrumentSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PriceSet'
ALTER TABLE [dbo].[PriceSet]
ADD CONSTRAINT [PK_PriceSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TradeSet'
ALTER TABLE [dbo].[TradeSet]
ADD CONSTRAINT [PK_TradeSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TotalSet'
ALTER TABLE [dbo].[TotalSet]
ADD CONSTRAINT [PK_TotalSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InterestRateSet'
ALTER TABLE [dbo].[InterestRateSet]
ADD CONSTRAINT [PK_InterestRateSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [InstType_Id] in table 'InstrumentSet'
ALTER TABLE [dbo].[InstrumentSet]
ADD CONSTRAINT [FK_InstTypeInstrument]
    FOREIGN KEY ([InstType_Id])
    REFERENCES [dbo].[InstTypeSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstTypeInstrument'
CREATE INDEX [IX_FK_InstTypeInstrument]
ON [dbo].[InstrumentSet]
    ([InstType_Id]);
GO

-- Creating foreign key on [Instrument_Id] in table 'PriceSet'
ALTER TABLE [dbo].[PriceSet]
ADD CONSTRAINT [FK_InstrumentPrice]
    FOREIGN KEY ([Instrument_Id])
    REFERENCES [dbo].[InstrumentSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstrumentPrice'
CREATE INDEX [IX_FK_InstrumentPrice]
ON [dbo].[PriceSet]
    ([Instrument_Id]);
GO

-- Creating foreign key on [Instrument_Id] in table 'TradeSet'
ALTER TABLE [dbo].[TradeSet]
ADD CONSTRAINT [FK_InstrumentTrade]
    FOREIGN KEY ([Instrument_Id])
    REFERENCES [dbo].[InstrumentSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstrumentTrade'
CREATE INDEX [IX_FK_InstrumentTrade]
ON [dbo].[TradeSet]
    ([Instrument_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------