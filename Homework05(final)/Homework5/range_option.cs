﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace Homework5
{
    class range_option : option
    {
        public static int cores = Environment.ProcessorCount;
        public range_option(double S, double K, double T, double r, double sigma, double N, double I, bool antithetic, bool cv, bool call, bool multithreading) : base(S, K, T, r, sigma, N, I, antithetic, cv, call, multithreading)
        {

        }
        public double simulate(double S, double K, double T, double r, double sigma, Int32 N, Int32 I, out double se)
        {//this is used to simulate the price of the european options
            double optionprice = 0;
            double sum = 0;//the sum value 
            double sum2 = 0;//the sum of the square value
            double sd = 0;
            if (I % cores != 0)
            {
                I = I + (cores - I % cores);
            }
            double[,] CT = new double[I, 2];
            int percore = Convert.ToInt32(I) / cores;//each core should have I/cores simulations.

            Action<object> simulation = x =>
            {
                double t = T / (N - 1);
                double cv_ = 0, Anti_cv = 0, delta1 = 0, delta2 = 0;
                double erddt = Math.Exp(r * t);
                double nudt = (r - sigma * sigma / 2) * t;
                double sigsdt = sigma * Math.Sqrt(t);
                double[,] sims = new double[I, N];
                double[,] anti_sims = new double[I, N];
                int start = Convert.ToInt32(x);
                int end = start + percore;
                double max_sims = S;
                double max_sims2 = S;
                double max_antisims = S;
                double max_antisims2 = S;
                double min_sims = S;
                double min_sims2 = S;
                double min_antisims = S;
                double min_antisims2 = S;
                for (int i = start; i < end; i++)
                {
                    cv_ = Anti_cv = 0;
                    sims[i, 0] = anti_sims[i, 0] = S;
                    max_sims = S;
                    max_sims2 = S;
                    max_antisims = S;
                    max_antisims2 = S;
                    min_sims = S;
                    min_sims2 = S;
                    min_antisims = S;
                    min_antisims2 = S;
                    for (int j = 1; j < N; j++)
                    {
                        sims[i, j] = sims[i, j - 1] * Math.Exp(nudt + sigsdt * MainWindow.random[i, j - 1]);
                        max_sims2 = Math.Max(sims[i, j], sims[i, j - 1]);
                        max_sims = Math.Max(max_sims2, max_sims);
                        min_sims2 = Math.Min(sims[i, j], sims[i, j - 1]);
                        min_sims = Math.Min(min_sims, min_sims2);
                        //this is the normal matrix.
                        if (cv)//control variate is used
                            {
                            delta1 = BS_delta(sims[i, j - 1], (N - j) * t, r, sigma);
                            cv_ = cv_ + delta1 * (sims[i, j] - sims[i, j - 1] * erddt);
                        }
                        if (antithetic)
                        {
                            anti_sims[i, j] = anti_sims[i, j - 1] * Math.Exp(nudt + sigsdt * (-1) * MainWindow.random[i, j - 1]);
                            max_antisims2 = Math.Max(anti_sims[i, j], anti_sims[i, j - 1]);
                            max_antisims = Math.Max(max_antisims2, max_antisims);
                            min_antisims2 = Math.Min(anti_sims[i, j], anti_sims[i, j - 1]);
                            min_antisims = Math.Min(min_antisims, min_antisims2);
                            //this is the antithetic opposite matrix
                            if (cv)
                            {
                                delta2 = BS_delta(anti_sims[i, j - 1], (N - j) * t, r, sigma);
                                Anti_cv = Anti_cv + delta2 * (anti_sims[i, j] - anti_sims[i, j - 1] * erddt);

                            }
                        }
                    }
                    
                        sims[i, N - 1] = max_sims-min_sims;
                       
                        anti_sims[i, N - 1] = max_antisims - min_antisims;
                       
                   
                    if (antithetic && !cv)//only antithetic
                        {
                        CT[i, 0] = (sims[i, N - 1] + anti_sims[i, N - 1]) / 2;
                    }
                    if (cv && !antithetic)//only control variate
                        {
                        CT[i, 0] = sims[i, N - 1] - cv_;
                    }
                    if (cv && antithetic)//both control variate and antithetic
                        {
                        CT[i, 0] = (sims[i, N - 1] + anti_sims[i, N - 1] - cv_ - Anti_cv) / 2;
                    }
                    if (!cv && !antithetic)//neither control variate either antithetic
                        {
                        CT[i, 0] = sims[i, N - 1];
                    }
                }

            };

            if (multithread)//the multithread is true, so we use the multithread way.
            {
                int count = 0;
                List<Thread> Threadlist = new List<Thread>();
                //this is a bunch of threads we will use in each core
                for (int i = 0; i < cores; i++)
                {
                    Thread th = new Thread(new ParameterizedThreadStart(simulation));
                    //new a thread called th
                    Threadlist.Add(th);
                    //add 8 th thread to the threadlist.
                    th.Start(count);
                    //start from 0.
                    count = count + percore;

                }
                foreach (Thread th in Threadlist)
                {
                    th.Join();
                }
                foreach (Thread th in Threadlist)
                {
                    th.Abort();
                }
                for (int i = 0; i < I; i++)
                {
                    sum = sum + CT[i, 0];
                    sum2 = sum2 + CT[i, 0] * CT[i, 0];
                }
                sd = Math.Sqrt((sum2 - sum * sum / I) * Math.Exp(-2 * r * T) / (I - 1));
                optionprice = sum / I * Math.Exp(-r * T);
                se = sd / Math.Sqrt(I);
                return optionprice;
            }
            else
            {
                percore = I;
                //we don't need to use multithreading
                Thread th = new Thread(new ParameterizedThreadStart(simulation));
                th.Start(0);
                th.Join();
                th.Abort();
                for (int i = 0; i < I; i++)
                {
                    sum = sum + CT[i, 0];
                    sum2 = sum2 + CT[i, 0] * CT[i, 0];
                }
                sd = Math.Sqrt((sum2 - sum * sum / I) * Math.Exp(-2 * r * T) / (I - 1));
                optionprice = sum / I * Math.Exp(-r * T);
                se = sd / Math.Sqrt(I);
                return optionprice;
            }
        }

        public static double NormDistFunc(double x)
        {
            //the code of the Standard Normal Distribution is from the CSDN.
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x) / Math.Sqrt(2.0);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return 0.5 * (1.0 + sign * y);
        }
        public double BS_delta(double S, double T, double r, double sigma)
        {//this function is used to calculate the delta value of the option, the formula comes from the Black-Scholes
            double d1 = 0;
            double delta = 0;
            d1 = (Math.Log(S / K) + (r + sigma * sigma / 2.0) * T) / (sigma * Math.Sqrt(T));
            if (call)
            {
                delta = NormDistFunc(d1);
            }
            else if (!call)
            {
                delta = NormDistFunc(d1) - 1;
            }
            return delta;
        }




    }
}
