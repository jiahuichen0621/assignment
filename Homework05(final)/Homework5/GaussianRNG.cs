﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Homework5
{
    public class GaussianRNG
    {

        public static int cores = Environment.ProcessorCount;
        private double I;
        private double N;
        public GaussianRNG(double I, double  N)
        {
            this.I = I;
            this.N = N;
        }
        ///it is used to generate normally distributed random numbers
        ///Gaussian(Box-Muller) Random Number Generator Class
        public static double box_muller(Random rnd)
        {
            double x1, x2, z1, z2;
            x1 = rnd.NextDouble();
            x2 = rnd.NextDouble();
            z1 = Math.Sqrt(-2 * Math.Log(x1)) * Math.Cos(2 * Math.PI * x2);
            z2 = Math.Sqrt(-2 * Math.Log(x1)) * Math.Sin(2 * Math.PI * x2);
            return z1;
        }
        public void randomgeneration()
        {//this is the normal road which doesn't use the multithreading
            Random rnd = new Random();
            for (int i = 0; i < I; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    MainWindow.random[i, j] = box_muller(rnd);
                }
            }
        }

        public void multithread()
        {//this is the road which use the multithreading
            if (I % cores != 0)
            {
                I = I + (cores - I % cores);
            }
            int percore = Convert.ToInt32(I) / cores;//each core should have percore
            Action<object> myACt = x =>
            {
                long para = Convert.ToInt32(x);
                Random rnd = new Random((int)DateTime.Now.Ticks);
                long start = para;
                long end = para + percore;
                for (long i = start; i < end; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        MainWindow.random[i, j] = box_muller(rnd);
                    }
                }
            };
            int count = 0;
            List<Thread> Threadlist = new List<Thread>();
            for (int i = 0; i < cores; i++)
            {
                Thread th = new Thread(new ParameterizedThreadStart(myACt));
                Threadlist.Add(th);
                th.Start(count);
                count = count + percore;
            }
            for (int i = 0; i < cores; i++)
            {
                foreach (Thread th in Threadlist)
                {
                    th.Join();
                }
                foreach (Thread th in Threadlist)
                {
                    th.Abort();
                }
            }
        }
    }
}
