﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Threading;
using System.Timers;

namespace Homework5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool antithetic, call, cv, multithread;
        Stopwatch watch = new Stopwatch();
        public static double[,] random;
        private delegate void UpdateProgressBarDelegate(System.Windows.DependencyProperty dp, Object value);
        private UpdateProgressBarDelegate m_update;
        public MainWindow()
        {
            InitializeComponent();
            ProgressBar1.Value = 0;
            ProgressBar1.Minimum = 0;
            m_update = new UpdateProgressBarDelegate(ProgressBar1.SetValue); ;
            this.kindofoption.Items.Add("EuropeanOption");
            this.kindofoption.Items.Add("AsianOption");
            this.kindofoption.Items.Add("DigitalOption");
            this.kindofoption.Items.Add("BarrierOption");
            this.kindofoption.Items.Add("LookbackOption");
            this.kindofoption.Items.Add("RangeOption");
            levelshow.Visibility = Visibility.Hidden;
            level_.Visibility = Visibility.Hidden;
            rebate.Visibility = Visibility.Hidden;
            rebateshow.Visibility = Visibility.Hidden;
            barrierkind.Visibility = Visibility.Hidden;
            barrierkindshow.Visibility = Visibility.Hidden; 
            this.barrierkind.Items.Add("down and out");
            this.barrierkind.Items.Add("down and in");
            this.barrierkind.Items.Add("up and out");
            this.barrierkind.Items.Add("up and in");
        }
       

        private void btnstart_Click(object sender, RoutedEventArgs e)
        {
            watch.Start();
            lbltimer.Content = "It is counting now......";
        }

        private void btnstop_Click(object sender, RoutedEventArgs e)
        {
            watch.Stop();
            lbltimer.Content = "Hours" + watch.Elapsed.Hours.ToString()
                + ": Minutes"
                + watch.Elapsed.Minutes.ToString()
                + ": seconds"
                + watch.Elapsed.Seconds.ToString()
                + ": millionseconds"
                + watch.Elapsed.Milliseconds.ToString();
        }

        private void btnreset_Click(object sender, RoutedEventArgs e)
        {
            watch.Reset();
            lbltimer.Content = "00:00:00:00";
        }
        private bool IsNumberic(string oText)
        {
            try
            {
                int var1 = Convert.ToInt32(oText);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void spotprice_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsNumberic(spotprice.Text) == false)
            {
                MessageBox.Show("please enter the true value!");
            }
        }
        private void strikeprice_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsNumberic(strikeprice.Text) == false)
            {
                MessageBox.Show("please enter the true value!");
            }
        }

        private void timetomaturity_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsNumberic(strikeprice.Text) == false)
            {
                MessageBox.Show("please enter the true value!");
            }
        }
        private void drift_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsNumberic(strikeprice.Text) == false)
            {
                MessageBox.Show("please enter the true value!");
            }
        }
        private void volatility_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsNumberic(strikeprice.Text) == false)
            {
                MessageBox.Show("please enter the true value!");
            }
        }
        private void steps_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsNumberic(strikeprice.Text) == false)
            {
                MessageBox.Show("please enter the true value!");
            }
        }
        private void kindofoption_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string option_kind = this.kindofoption.SelectedItem.ToString();
            if (option_kind=="DigitalOption")
            {
                rebate.Visibility = Visibility.Visible;
                rebateshow.Visibility = Visibility.Visible;
            }
            else
            {
                rebate.Visibility = Visibility.Hidden;
                rebateshow.Visibility = Visibility.Hidden;
            }
            if (option_kind=="BarrierOption")
            {
                
                levelshow.Visibility = Visibility.Visible;
                level_.Visibility = Visibility.Visible;
                barrierkind.Visibility = Visibility.Visible;
                barrierkindshow.Visibility = Visibility.Visible;
            }
            else
            {
                levelshow.Visibility = Visibility.Hidden;
                level_.Visibility = Visibility.Hidden;
                barrierkind.Visibility = Visibility.Hidden;
                barrierkindshow.Visibility = Visibility.Hidden;
            }
           
            if(option_kind=="RangeOption")
            {
                calloption.Visibility = Visibility.Hidden;
                putoption.Visibility = Visibility.Hidden;
            }
            else
            {
                calloption.Visibility = Visibility.Visible;
                putoption.Visibility = Visibility.Visible;
            }

        }

        private void barrierkind_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void numberofsimulations_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsNumberic(strikeprice.Text) == false)
            {
                MessageBox.Show("please enter the true value!");
            }
        }

        
        private void updateProgress(int value)
        {
            Dispatcher.Invoke(m_update, System.Windows.Threading.DispatcherPriority.Background, new object[] { System.Windows.Controls.ProgressBar.ValueProperty, Convert.ToDouble(value) });
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            updateProgress(0);
            watch.Reset();
            lbltimer.Content = "00:00:00:00";
            watch.Start();
            double[] price = new double[4];
            double S = Convert.ToDouble(spotprice.Text);
            double K = Convert.ToDouble(strikeprice.Text);
            double T = Convert.ToDouble(timetomaturity.Text);
            double r = Convert.ToDouble(drift.Text);
            double sigma = Convert.ToDouble(volatility.Text);
            Int32 N = Convert.ToInt32(steps.Text);
            Int32 I = Convert.ToInt32(numberofsimulations.Text);
            
            bool antithetic = false, call = false, cv = false, multithread = false;
            string option_kind = this.kindofoption.SelectedItem.ToString();
           
            if (multithread_checked.IsChecked==false)
            {
                I = Convert.ToInt32(numberofsimulations.Text);
                if (I % 8 !=0)
                {
                    I = I + (8 - I % 8);
                }

            }
            else
            {
                I = Convert.ToInt32(numberofsimulations.Text);
                if(I % 8 !=0)
                {
                    I = I + (8 - I % 8);
                }
            }
            if (anti_checked.IsChecked == true)
            {
                antithetic = true;
            }
            else
            {
                antithetic = false;
            }
            if (calloption.IsChecked == true)
            {
                call = true;
            }
            if (putoption.IsChecked == true)
            {
                call = false;
            }
            if (cv_checked.IsChecked == true)
            {
                cv = true;
            }
            else
            {
                cv = false;
            }
            if (multithread_checked.IsChecked == true)
            {
                multithread = true;
            }
            else
            {
                multithread = false;
            }
            double optionprice;
            option op = new option(S, K, T, r, sigma, N, I, antithetic, cv, call, multithread);
            GaussianRNG rnd = new GaussianRNG(I, N);
            random = new double[I, N];
            if (multithread)
            {
                rnd.multithread();
            }
            else
            {
                rnd.randomgeneration();
            }
            
            ProgressBar1.Value = 4;
            
            if (option_kind=="EuropeanOption")
            {
                double se = 0;
                double delta = 0, gamma = 0, theta = 0, vega = 0, rho = 0;
                europeanoption simulator = new europeanoption(S, K, T, r, sigma, N, I, antithetic, cv, call, multithread);
                optionprice = Math.Round(simulator.simulate(S, K, T, r, sigma, N, I, out se), 5);
                updateProgress(25);
                delta = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, out se) - simulator.simulate(0.999 * S, K, T, r, sigma, N, I, out se)) / (0.001 * 2 * S), 5);
                updateProgress(50);
                gamma = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, out se) - 2 * optionprice + simulator.simulate(0.999 * S, K, T, r, sigma, N, I, out se)) / (0.001 * S * 0.001 * S), 5);
                updateProgress(60);
                theta = Math.Round(-(simulator.simulate(S, K, T * 1.001, r, sigma, N, I, out se) - optionprice) / (0.001 * T), 5);
                updateProgress(70);
                vega = Math.Round((simulator.simulate(S, K, T, r, sigma + 0.001 * sigma, N, I, out se) - simulator.simulate(S, K, T, r, sigma - 0.001 * sigma, N, I, out se)) / (2 * 0.001 * sigma), 5);
                updateProgress(80);
                rho = Math.Round((simulator.simulate(S, K, T, r * 1.001, sigma, N, I, out se) - simulator.simulate(S, K, T, r * 0.999, sigma, N, I, out se)) / (2 * 0.001 * r), 5);
                updateProgress(100);
                ResultShow.Content = "For European option:" + System.Environment.NewLine + "the option value:   " + Convert.ToString(optionprice) + System.Environment.NewLine
                        + "the stand error:   " + Convert.ToString(se) + System.Environment.NewLine + "the delta:   " + Convert.ToString(delta) + System.Environment.NewLine
                        + "the gamma:   " + Convert.ToString(gamma) + System.Environment.NewLine + "the vega:   " + Convert.ToString(vega) + System.Environment.NewLine
                        + "the theta:   " + Convert.ToString(theta) + System.Environment.NewLine + "the Rho   " + Convert.ToString(rho) + System.Environment.NewLine + "The Number Of Cores is   " + Convert.ToString(europeanoption.cores);
                lbltimer.Content = "Hours" + watch.Elapsed.Hours.ToString()
                     + ": Minutes"
                     + watch.Elapsed.Minutes.ToString()
                     + ": seconds"
                     + watch.Elapsed.Seconds.ToString()
                     + ": millionseconds"
                     + watch.Elapsed.Milliseconds.ToString();
            }
            if (option_kind=="AsianOption")
            {
                double se = 0;
                double delta = 0, gamma = 0, theta = 0, vega = 0, rho = 0;
                asianoption simulator = new asianoption(S, K, T, r, sigma, N, I, antithetic, cv, call, multithread);
                optionprice = Math.Round(simulator.simulate(S, K, T, r, sigma, N, I, out se), 5);
                updateProgress(25);
                delta = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, out se) - simulator.simulate(0.999 * S, K, T, r, sigma, N, I, out se)) / (0.001 * 2 * S), 5);
                updateProgress(50);
                gamma = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, out se) - 2 * optionprice + simulator.simulate(0.999 * S, K, T, r, sigma, N, I, out se)) / (0.001 * S * 0.001 * S), 5);
                updateProgress(60);
                theta = Math.Round(-(simulator.simulate(S, K, T * 1.001, r, sigma, N, I, out se) - optionprice) / (0.001 * T), 5);
                updateProgress(70);
                vega = Math.Round((simulator.simulate(S, K, T, r, sigma + 0.001 * sigma, N, I, out se) - simulator.simulate(S, K, T, r, sigma - 0.001 * sigma, N, I, out se)) / (2 * 0.001 * sigma), 5);
                updateProgress(80);
                rho = Math.Round((simulator.simulate(S, K, T, r * 1.001, sigma, N, I, out se) - simulator.simulate(S, K, T, r * 0.999, sigma, N, I, out se)) / (2 * 0.001 * r), 5);
                updateProgress(100);
                ResultShow.Content = "For Asian option:" + System.Environment.NewLine + "the option value:   " + Convert.ToString(optionprice) + System.Environment.NewLine
                        + "the stand error:   " + Convert.ToString(se) + System.Environment.NewLine + "the delta:   " + Convert.ToString(delta) + System.Environment.NewLine
                        + "the gamma:   " + Convert.ToString(gamma) + System.Environment.NewLine + "the vega:   " + Convert.ToString(vega) + System.Environment.NewLine
                        + "the theta:   " + Convert.ToString(theta) + System.Environment.NewLine + "the Rho   " + Convert.ToString(rho) + System.Environment.NewLine + "The Number Of Cores is   " + Convert.ToString(europeanoption.cores);
                lbltimer.Content = "Hours" + watch.Elapsed.Hours.ToString()
                     + ": Minutes"
                     + watch.Elapsed.Minutes.ToString()
                     + ": seconds"
                     + watch.Elapsed.Seconds.ToString()
                     + ": millionseconds"
                     + watch.Elapsed.Milliseconds.ToString();

            }
            if (option_kind=="DigitalOption")
            {
                double se = 0;
                double delta = 0, gamma = 0, theta = 0, vega = 0, rho = 0;
                double rebate = Convert.ToDouble(rebateshow.Text);
                digitaloption simulator = new digitaloption(S, K, T, r, sigma, N, I, antithetic, cv, call, multithread, rebate);
                optionprice = Math.Round(simulator.simulate(S, K, T, r, sigma, N, I, rebate, out se), 5);
                updateProgress(25);
                delta = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, rebate, out se) - simulator.simulate(0.999 * S, K, T, r, sigma, N, I, rebate, out se)) / (0.001 * 2 * S), 5);
                updateProgress(50);
                gamma = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, rebate, out se) - 2 * optionprice + simulator.simulate(0.999 * S, K, T, r, sigma, N, I, rebate, out se)) / (0.001 * S * 0.001 * S), 5);
                updateProgress(60);
                theta = Math.Round(-(simulator.simulate(S, K, T * 1.001, r, sigma, N, I, rebate, out se) - optionprice) / (0.001 * T), 5);
                updateProgress(70);
                vega = Math.Round((simulator.simulate(S, K, T, r, sigma + 0.001 * sigma, N, I, rebate, out se) - simulator.simulate(S, K, T, r, sigma - 0.001 * sigma, N, I, rebate, out se)) / (2 * 0.001 * sigma), 5);
                updateProgress(80);
                rho = Math.Round((simulator.simulate(S, K, T, r * 1.001, sigma, N, I, rebate, out se) - simulator.simulate(S, K, T, r * 0.999, sigma, N, I, rebate, out se)) / (2 * 0.001 * r), 5);
                updateProgress(100);
                ResultShow.Content = "For Digital option:" + System.Environment.NewLine + "the option value:   " + Convert.ToString(optionprice) + System.Environment.NewLine
                        + "the stand error:   " + Convert.ToString(se) + System.Environment.NewLine + "the delta:   " + Convert.ToString(delta) + System.Environment.NewLine
                        + "the gamma:   " + Convert.ToString(gamma) + System.Environment.NewLine + "the vega:   " + Convert.ToString(vega) + System.Environment.NewLine
                        + "the theta:   " + Convert.ToString(theta) + System.Environment.NewLine + "the Rho   " + Convert.ToString(rho) + System.Environment.NewLine + "The Number Of Cores is   " + Convert.ToString(europeanoption.cores);
                lbltimer.Content = "Hours" + watch.Elapsed.Hours.ToString()
                     + ": Minutes"
                     + watch.Elapsed.Minutes.ToString()
                     + ": seconds"
                     + watch.Elapsed.Seconds.ToString()
                     + ": millionseconds"
                     + watch.Elapsed.Milliseconds.ToString();

            }
            if(option_kind=="BarrierOption")
            {
                double se = 0;
                double delta = 0, gamma = 0, theta = 0, vega = 0, rho = 0;
                string type = this.barrierkind.SelectedValue.ToString();
                double level = Convert.ToDouble(level_.Text);
                barrieroption simulator = new barrieroption(S, K, T, r, sigma, N, I, level, type, antithetic, cv, call, multithread);
                optionprice = Math.Round(simulator.simulate(S, K, T, r, sigma, N, I, out se), 5);
                updateProgress(25);
                delta = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, out se) - simulator.simulate(0.999 * S, K, T, r, sigma, N, I, out se)) / (0.001 * 2 * S), 5);
                updateProgress(50);
                gamma = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, out se) - 2 * optionprice + simulator.simulate(0.999 * S, K, T, r, sigma, N, I, out se)) / (0.001 * S * 0.001 * S), 5);
                updateProgress(60);
                theta = Math.Round(-(simulator.simulate(S, K, T * 1.001, r, sigma, N, I, out se) - optionprice) / (0.001 * T), 5);
                updateProgress(70);
                vega = Math.Round((simulator.simulate(S, K, T, r, sigma + 0.001 * sigma, N, I, out se) - simulator.simulate(S, K, T, r, sigma - 0.001 * sigma, N, I, out se)) / (2 * 0.001 * sigma), 5);
                updateProgress(80);
                rho = Math.Round((simulator.simulate(S, K, T, r * 1.001, sigma, N, I, out se) - simulator.simulate(S, K, T, r * 0.999, sigma, N, I, out se)) / (2 * 0.001 * r), 5);
                updateProgress(100);
                ResultShow.Content = "For Barrier option:" + System.Environment.NewLine + "the option value:   " + Convert.ToString(optionprice) + System.Environment.NewLine
                        + "the stand error:   " + Convert.ToString(se) + System.Environment.NewLine + "the delta:   " + Convert.ToString(delta) + System.Environment.NewLine
                        + "the gamma:   " + Convert.ToString(gamma) + System.Environment.NewLine + "the vega:   " + Convert.ToString(vega) + System.Environment.NewLine
                        + "the theta:   " + Convert.ToString(theta) + System.Environment.NewLine + "the Rho   " + Convert.ToString(rho) + System.Environment.NewLine + "The Number Of Cores is   " + Convert.ToString(europeanoption.cores)
                        +"the barrier option"+ type ;
                lbltimer.Content = "Hours" + watch.Elapsed.Hours.ToString()
                     + ": Minutes"
                     + watch.Elapsed.Minutes.ToString()
                     + ": seconds"
                     + watch.Elapsed.Seconds.ToString()
                     + ": millionseconds"
                     + watch.Elapsed.Milliseconds.ToString();
            }
            if(option_kind=="LookbackOption")
            {
                double se = 0;
                double delta = 0, gamma = 0, theta = 0, vega = 0, rho = 0;
                lookbackoption simulator = new lookbackoption(S, K, T, r, sigma, N, I, antithetic, cv, call, multithread);
                optionprice = Math.Round(simulator.simulate(S, K, T, r, sigma, N, I, out se), 5);
                updateProgress(25);
                delta = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, out se) - simulator.simulate(0.999 * S, K, T, r, sigma, N, I, out se)) / (0.001 * 2 * S), 5);
                updateProgress(50);
                gamma = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, out se) - 2 * optionprice + simulator.simulate(0.999 * S, K, T, r, sigma, N, I, out se)) / (0.001 * S * 0.001 * S), 5);
                updateProgress(60);
                theta = Math.Round(-(simulator.simulate(S, K, T * 1.001, r, sigma, N, I, out se) - optionprice) / (0.001 * T), 5);
                updateProgress(70);
                vega = Math.Round((simulator.simulate(S, K, T, r, sigma + 0.001 * sigma, N, I, out se) - simulator.simulate(S, K, T, r, sigma - 0.001 * sigma, N, I, out se)) / (2 * 0.001 * sigma), 5);
                updateProgress(80);
                rho = Math.Round((simulator.simulate(S, K, T, r * 1.001, sigma, N, I, out se) - simulator.simulate(S, K, T, r * 0.999, sigma, N, I, out se)) / (2 * 0.001 * r), 5);
                updateProgress(100);
                ResultShow.Content = "For LookBack option:" + System.Environment.NewLine + "the option value:   " + Convert.ToString(optionprice) + System.Environment.NewLine
                        + "the stand error:   " + Convert.ToString(se) + System.Environment.NewLine + "the delta:   " + Convert.ToString(delta) + System.Environment.NewLine
                        + "the gamma:   " + Convert.ToString(gamma) + System.Environment.NewLine + "the vega:   " + Convert.ToString(vega) + System.Environment.NewLine
                        + "the theta:   " + Convert.ToString(theta) + System.Environment.NewLine + "the Rho   " + Convert.ToString(rho) + System.Environment.NewLine + "The Number Of Cores is   " + Convert.ToString(europeanoption.cores);
                lbltimer.Content = "Hours" + watch.Elapsed.Hours.ToString()
                     + ": Minutes"
                     + watch.Elapsed.Minutes.ToString()
                     + ": seconds"
                     + watch.Elapsed.Seconds.ToString()
                     + ": millionseconds"
                     + watch.Elapsed.Milliseconds.ToString();
            }
            if(option_kind=="RangeOption")
            {
                double se = 0;
                double delta = 0, gamma = 0, theta = 0, vega = 0, rho = 0;
                call = true;
                range_option simulator = new range_option(S, K, T, r, sigma, N, I, antithetic, cv, call, multithread);
                optionprice = Math.Round(simulator.simulate(S, K, T, r, sigma, N, I, out se), 5);
                updateProgress(25);
                delta = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, out se) - simulator.simulate(0.999 * S, K, T, r, sigma, N, I, out se)) / (0.001 * 2 * S), 5);
                updateProgress(50);
                gamma = Math.Round((simulator.simulate(1.001 * S, K, T, r, sigma, N, I, out se) - 2 * optionprice + simulator.simulate(0.999 * S, K, T, r, sigma, N, I, out se)) / (0.001 * S * 0.001 * S), 5);
                updateProgress(60);
                theta = Math.Round(-(simulator.simulate(S, K, T * 1.001, r, sigma, N, I, out se) - optionprice) / (0.001 * T), 5);
                updateProgress(70);
                vega = Math.Round((simulator.simulate(S, K, T, r, sigma + 0.001 * sigma, N, I, out se) - simulator.simulate(S, K, T, r, sigma - 0.001 * sigma, N, I, out se)) / (2 * 0.001 * sigma), 5);
                updateProgress(80);
                rho = Math.Round((simulator.simulate(S, K, T, r * 1.001, sigma, N, I, out se) - simulator.simulate(S, K, T, r * 0.999, sigma, N, I, out se)) / (2 * 0.001 * r), 5);
                updateProgress(100);
                ResultShow.Content = "For Range option:" + System.Environment.NewLine + "the option value:   " + Convert.ToString(optionprice) + System.Environment.NewLine
                        + "the stand error:   " + Convert.ToString(se) + System.Environment.NewLine + "the delta:   " + Convert.ToString(delta) + System.Environment.NewLine
                        + "the gamma:   " + Convert.ToString(gamma) + System.Environment.NewLine + "the vega:   " + Convert.ToString(vega) + System.Environment.NewLine
                        + "the theta:   " + Convert.ToString(theta) + System.Environment.NewLine + "the Rho   " + Convert.ToString(rho) + System.Environment.NewLine + "The Number Of Cores is   " + Convert.ToString(europeanoption.cores);
                lbltimer.Content = "Hours" + watch.Elapsed.Hours.ToString()
                     + ": Minutes"
                     + watch.Elapsed.Minutes.ToString()
                     + ": seconds"
                     + watch.Elapsed.Seconds.ToString()
                     + ": millionseconds"
                     + watch.Elapsed.Milliseconds.ToString();
            }
            
                
            
        }
      

    }
}
