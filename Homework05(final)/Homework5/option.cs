﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework5
{
    class option
    {
        public double S;
        public double K;
        public double r;
        public double T;
        public double sigma;
        public double N;
        public double I;
        public bool antithetic;
        public bool cv;
        public bool multithread;
        public bool call;

        public option(double S,double K, double r, double T, double sigma, double N, double I, bool antithetic, bool cv, bool call, bool multithread)
        {
            this.S = S;
            this.K = K;
            this.r = r;
            this.T = T;
            this.sigma = sigma;
            this.N = N;
            this.I = I;
            this.antithetic = antithetic;
            this.cv = cv;
            this.call = call;
            this.multithread = multithread;
        }

    }
}
